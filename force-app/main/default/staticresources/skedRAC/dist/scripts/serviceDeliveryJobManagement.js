(function(angular) {

  var app = angular.module('app', ['app.jobCreation', 'app.api'])

  app.controller('ServiceDeliveryJobManagementCtrl', ['$scope', 'api', function($scope, api) {

    $scope.state = {
      openJobCreationDialog: false
    }

    $scope.createJob = function () {
      $scope.state.openJobCreationDialog = true
    }

    $scope.hideJobCreationDialog = function () {
      $scope.state.openJobCreationDialog = false
    }

    $scope.handleSaveJobSuccess = function (result) {
      console.log('handleSaveJobSuccess', result)
      $scope.$emit('jobCreation:success', result)
    }

    $scope.$on('jobCreation:success', function (ev, data) {
      // update job list
    })

    $scope.init = function () {
      // api.getServiceDelivery: populate $scope.data.ServiceDelivery
      // api.getJobDetails: populate $scope.data.JobDetails
    }

    $scope.init()

    $scope.data = {
      ServiceDelivery: {
        CustomerAccount: "CBRE",
        ContactDetails: "Details 1",
        ServiceDeliveryName: "Curtin University",
        StartDate: new Date(),
        DueDate: new Date(),
        Status: "Pending Allocation",
        Category: "NSW - Logistics",
        Region: "NSW"
      },
      JobDetails: [{
        Subject: "Logistics - Crate Delivery",
        Date: new Date(),
        JobNumber: "JOB-0001",
        Resources: "6",
        AllocationStatus: "Fully Allocated", // green background
        JobStatus: "Pending Dispatch",
        TlResource: {
          firstName: "Devon",
          lastName: "Jacons"
        },
        OtherAllocatedResources: [{
          firstName: "Joe",
          lastName: "Smith"
        }, {
          firstName: "Frank",
          lastName: "Doe"
        }, {
          firstName: "Edith",
          lastName: "Rankins"
        }, {
          firstName: "Michael",
          lastName: "Bright"
        }, {
          firstName: "Bryan",
          lastName: "Godrey"
        }]
      }, {
        Subject: "Logistics - Driving - 2nd August / Toolers",
        Date: new Date(),
        JobNumber: "JOB-0002",
        Resources: "3",
        AllocationStatus: "Partially Allocated", // yellow background
        JobStatus: "Pending Allocation",
        TlResource: {
          firstName: "Bob",
          lastName: "Hope"
        },
        OtherAllocatedResources: []
      }, {
        Subject: "Logistics - Relocation - 2nd August / Packers",
        Date: new Date(),
        JobNumber: "JOB-0003",
        Resources: "2",
        AllocationStatus: "No Allocation", // pink background
        JobStatus: "Pending Allocation",
        TlResource: null,
        OtherAllocatedResources: []
      }]
    };

  }])

})(angular);

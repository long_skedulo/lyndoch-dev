public class skedCustomMetaDataSetting {
    // Key is the developername
    private static Map<String, sked_Interface_Handler_Settings__mdt> mpSkedInterfaceHandlerSetting = null;

    /**
    *@description Singleton to get sked Interface Handler setting
    *
    */
    public static Map<String, sked_Interface_Handler_Settings__mdt> getskedInterfaceHandlerSetting() {
        if(mpSkedInterfaceHandlerSetting == null) {
            mpSkedInterfaceHandlerSetting = new Map<String, sked_Interface_Handler_Settings__mdt>();
            for (sked_Interface_Handler_Settings__mdt hdlSetting : [
                    SELECT Id, Handler_Name__c, Active__c, DeveloperName
                    FROM sked_Interface_Handler_Settings__mdt
                ]
            ) {
                mpSkedInterfaceHandlerSetting.put(hdlSetting.DeveloperName, hdlSetting);
            }
        }
        return mpSkedInterfaceHandlerSetting;
    }

}
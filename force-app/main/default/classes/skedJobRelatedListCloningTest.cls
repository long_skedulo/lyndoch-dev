@isTest
public class skedJobRelatedListCloningTest {
	@testSetup
    static void initData() {
        skedTestDataFactory.setupCustomSettings();
        Account account = skedTestDataFactory.createAccounts('Test Account', 1).get(0);
        insert account;		        
        
        sked__Region__c region = skedTestDataFactory.createRegion('Sydney','Australia/Sydney');
        insert region;
		
        Contact client = skedTestDataFactory.createContact(account.Id, 'Test Client');
        insert client;
        
        sked__Location__c location = skedTestDataFactory.createLocation('Test Location', account.Id, region.Id);
        location.sked__GeoLocation__Latitude__s = -27.457730;
        location.sked__GeoLocation__Longitude__s = 153.037080;
        insert location;
        
        Product2 product = skedTestDataFactory.createProduct('Test Product', 'P1');
        insert product;
		
        sked__Job__c job = skedTestDataFactory.createJobs(account.Id, region.Id, null, 2).get(0);
        job.sked__Contact__c = client.Id;
        job.sked__Job_Status__c = 'Pending Allocation';
        job.sked__GeoLocation__Latitude__s = location.sked__GeoLocation__Latitude__s;
        job.sked__GeoLocation__Longitude__s = location.sked__GeoLocation__Longitude__s;
        insert job;
		
        sked__Job_Product__c jobProd = skedTestDataFactory.createJobProduct(job.Id, product.Id, 1);
        insert jobProd;

        sked__Resource__c resource = skedTestDataFactory.createResource('Test', UserInfo.getUserId(), region.Id);
        insert resource;

        sked__Job_Allocation__c ja = skedTestDataFactory.allocateJob(job.Id, resource.Id);
        ja.sked__Status__c = 'Confirmed';
        insert ja;
    }
    
    @isTest static void testCloneJobRelatedList() {
        sked__Job__c mainJob = [select id, sked__Recurring_Schedule__c from sked__Job__c limit 1];
        Account account = [select id from account limit 1];
        sked__Region__c region = [select id from sked__Region__c limit 1];   
        Contact client = [select id from Contact limit 1];
        sked__Resource__c resource = [select id from sked__Resource__c limit 1];
        // Create recurring schedule
        sked__Recurring_Schedule__c recurringSchedule = new sked__Recurring_Schedule__c(
            sked__Description__c = 'test',
            sked__Summary__c = 'test'
        );
        
        insert recurringSchedule;

        mainJob.sked__Recurring_Schedule__c = recurringSchedule.Id;
        update mainJob;
        Test.startTest();        
        List<sked__Job__c> recurJobs = skedTestDataFactory.createJobs(account.Id, region.Id, null, 2);
        for(sked__Job__c job : recurJobs){
            job.sked__Contact__c = client.Id;
            job.sked__Recurring_Schedule__c = recurringSchedule.Id;
        }
        insert recurJobs;                        
        Test.stopTest();                
    }

}
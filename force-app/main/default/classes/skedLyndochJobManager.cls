public class skedLyndochJobManager extends skedAsyncJobManager {
	private static skedIAsyncJobManager instance;

    public static skedIAsyncJobManager newInstance(){
        if(instance == null) instance = new skedLyndochJobManager();
        return instance;
    }

    //Return action map
    public override Map<String,String> getActionMap(){
        return new Map<String,String>{
            skedConstants.CLONE_JOB_RELATED_LIST      => 'skedLyndochJobManager.JobRelatedListCloning'            
        };
    }

    //======= EXCUTION  METHODS =======
    
    public class JobRelatedListCloning extends skedAsyncJobManager{
        //Start a job
        public override void startJob(skedAsyncJob job){
            super.startJob(job);
            //startNow(job);
            startAfter(job, skedConfigs.RECURRING_JOB_REPLICATION_DELAY_TIME);
            //startAsBatch(job);
            //startSerialJobAfter(job, skedConfigs.SMS_DELAY_TIME);
        }

        //Execute the job
         public override void executeJob(skedAsyncJob job){
            super.executeJob(job);
            //System.debug('job.param#' + job.param);
            Set<Id> jobIdsWithRecurring = (Set<Id>)job.param;
            skedRecurJobRelatedListCloning recJobClonning = new skedRecurJobRelatedListCloning();
        	recJobClonning.cloneJobRealtedListForRecurringJobs(jobIdsWithRecurring);
        }               
    }
}
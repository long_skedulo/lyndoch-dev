public class skedConstants {
	public static final String REGION_WESTERN_AUSTRALIA = 'Western Australia';
    public static final String STATUS_CURRENT = 'Current';
    public static final String TIMESHEET_STATUS_DRAFT = 'Draft';
    public static final String TIMESHEET_STATUS_PENDING_APPROVAL = 'Pending Approval';
    public static final String TIMESHEET_STATUS_APPROVED = 'Approved';
    public static final String TIMESHEET_STATUS_REJECTED = 'Rejected';
    public static final String JOB_ALLOCATION_DECLINED = 'Declined';
    public static final String JOB_ALLOCATION_DELETED = 'Deleted';
    public static final String JOB_ALLOCATION_MODIFIED = 'Modified';

    public static final string AVAILABILITY_STATUS_APPROVED = 'Approved';
    public static final string AVAILABILITY_STATUS_PENDING = 'Pending';
    public static final string AVAILABILITY_STATUS_DECLINED = 'Declined';

    public static final string AVAILABILITY_TYPE_AVAILABLE = 'Available';

    public static final string JOB_STATUS_CANCELLED = 'Cancelled';
    public static final string JOB_STATUS_COMPLETE = 'Complete';
    public static final string JOB_STATUS_DISPATCHED = 'Dispatched';
    public static final string JOB_STATUS_IN_PROGRESS = 'In Progress';
    public static final string JOB_STATUS_PENDING_ALLOCATION = 'Pending Allocation';
    public static final string JOB_STATUS_PENDING_DISPATCH = 'Pending Dispatch';
    public static final string JOB_STATUS_QUEUED = 'Queued';
    public static final string JOB_STATUS_READY = 'Ready';
    public static final string JOB_STATUS_EN_ROUTE = 'En Route';
    public static final string JOB_STATUS_ON_SITE = 'On Site';

    public static final List<string> JOB_STATUSES_IN_ORDER = new List<String>{
        JOB_STATUS_QUEUED, JOB_STATUS_PENDING_ALLOCATION, JOB_STATUS_PENDING_DISPATCH,
        JOB_STATUS_DISPATCHED, JOB_STATUS_READY, JOB_STATUS_EN_ROUTE, JOB_STATUS_ON_SITE, JOB_STATUS_COMPLETE
    };

    public static final string JOB_ALLOCATION_STATUS_COMPLETE = 'Complete';
    public static final string JOB_ALLOCATION_STATUS_CONFIRMED = 'Confirmed';
    public static final string JOB_ALLOCATION_STATUS_DECLINED = 'Declined';
    public static final string JOB_ALLOCATION_STATUS_DELETED = 'Deleted';
    public static final string JOB_ALLOCATION_STATUS_DISPATCHED = 'Dispatched';
    public static final string JOB_ALLOCATION_STATUS_PENDING_DISPATCH = 'Pending Dispatch';
    public static final string JOB_ALLOCATION_STATUS_IN_PROGRESS = 'In Progress';
    public static final string JOB_ALLOCATION_STATUS_EN_ROUTE = 'En Route';
    public static final string JOB_ALLOCATION_STATUS_CHECKED_IN = 'Checked In';
    public static final string JOB_ALLOCATION_STATUS_ON_SITE = 'On Site'; // On site = Checked In

	public static final String GROUP_EVENT_STATUS_ACTIVE = 'Active';

    public static final string JOB_ABORT_REASON_CANCEL_WITH_NOTICE = 'Cancelled with Notice';
    public static final string JOB_ABORT_REASON_DID_NOT_ATTEND = 'Did Not Attend';

    public static final string REGION_CENTRAL_COAST_DISABILITY = 'Central Coast Disability';
    public static final string REGION_QUEENSLAND = 'Queensland';

    public static final string PUBLIC_GROUP_QUEENSLAND_ALERT = 'Queensland Alert';
    public static final string PUBLIC_GROUP_CENTRAL_COAST_ALERT = 'Central Coast Alert';

    public static final string JOB_TYPE_SINGLE_BOOKING = 'Single Booking';
    public static final string JOB_TYPE_GROUP_EVENT = 'Group Event';

    public static final string JOB_NOT_ENOUGH_RES = 'Too many resources have been allocated, not enough participants have been assigned to job';

    public static final string SHIFT_STATUS_PENDING = 'Pending';
    public static final string SHIFT_STATUS_ACTIVE = 'Active';
    public static final string SHIFT_STATUS_COMPLETE = 'Complete';
    public static final string SHIFT_STATUS_FINALISED = 'Finalised';

    // Roster Console Error
    public static final string JOB_VIOLATION = 'Job Violation';
    public static final string RES_UNAVAILABLE = 'Resource Unavailable';
    public static final string RES_UNAVAILABLE_ERROR_CONTENT = ' is not available at this time';
    public static final string JOB_GAP_ALLOCATION_COVERAGE_ERROR = 'Job Duration Gap';
    public static final string JOB_GAP_ALLOCATION_COVERAGE_ERROR_CONTENT = 'The allocated Resources against this Job do not cover the full duration';
    // Resource Type
    public static final String RESOURCE_TYPE_PERSON = 'Person';
    public static final string RESOURCE_TYPE_ASSET = 'Asset';
    public static final String RESOURCE_FULL_TIME_EMPLOYMENT = 'Full-time';

    // Service Agreement Validation error
    public static final String SERVICE_ITEM_NOT_EXIST = 'Service agreement item does not exist';
    public static final String SERVICE_ITEM_VALIDATIOIN_ERROR = 'Validation failed on one of selected services, please review them';

    // Client Availability Validation
    public static final String CLIENT_NOT_AVAILABLE = 'The time you selected for this job falls outside the clients availability. Do you wish to proceed?';
    // Interface General ---------------------------------------------
    public static final string HOLIDAY_GLOBAL = 'global';
    public static final String INVALID_ADDRESS_ERROR_MSG = 'Cannot get geolocation from this address!';
    public static final String ADDRESS_TYPE_LOCATION = 'Location';
    public static final String ADDRESS_TYPE_SITE = 'Site';
    public static final String CLIENT_BLACKLIST_MESSAGE = 'This client is unable to attend due to conflict with: ';

    // SF Object Name
    public static final String OBJECTNAME_GROUP_CLIENT = 'GROUP_CLIENT__C';
    public static final String OBJECTNAME_JOB_SERVICE_ITEM = 'JOB_SERVICE_ITEM__C';

    // Enrite related
    public static final String UNCATEGORISED_CATEGORY = 'Uncategorised';

    // Contact record type
    public static final String CONTACT_RECORDTYPE_EMPLOYEE = 'Employee';
    public static final String CONTACT_RECORDTYPE_CLIENT = 'Client';

    // Event Types
    public static final String EVENT_TYPE_JOB = 'job';
    public static final String EVENT_TYPE_AVAILABILITY = 'availability';
    public static final String EVENT_TYPE_UNAVAILABILITY = 'un-availability';
    public static final String EVENT_TYPE_JOB_ALLOCATION = 'allocation';
    public static final String EVENT_TYPE_ACTIVITY = 'activity';
    public static final String EVENT_TYPE_SHIFT = 'shift';
    public static final String EVENT_TYPE_NOT_AVAILABLE = 'non-working';
    // Interface developername ---------------------------------------------
    public static final string AVAILATOR_DEVELOPERNAME = 'skedAvailator';
    public static final string RESOURCE_COSTING_DEVELOPERNAME = 'ResourceCosting';
    public static final string ROLE_DEVELOPERNAME = 'Role';
    public static final string TIMESHEET_SHIFT_DEVELOPERNAME = 'TimeSheetShift';
    public static final string ROSTER_SHIFT_SERVICE_DEVELOPERNAME = 'RosterShiftService';
    public static final string SINGLE_EVENT_CTRL_DEVELOPERNAME = 'SingleEventCtrlHandler';
    public static final string GROUP_EVENT_CTRL_DEVELOPERNAME = 'GroupEventCtrlHandler';
    public static final string RECURRING_JOB_CLONING_CTRL_DEVELOPERNAME = 'RecurringJobCloningHandler';
    public static final string REPLICATION_JOB_CLONING_CTRL_DEVELOPERNAME = 'ReplicationJobCloningHandler';
    public static final string TIMESHEET_CTRL_DEVELOPERNAME = 'TimesheetCtrlHandler';

    // DATE AND BALANCE CHECK
    public static final String RESOLUTION_COVERED_JOBS = 'COVERED JOBS';
    public static final String RESOLUTION_NO_JOBS = 'NO JOBS';
    public static final String RESOLUTION_ALL_JOBS = 'ALL JOBS';

    //Offer status
    public static final String OFFER_ACCEPT = 'Accepted';
    public static final String OFFER_DECLINED = 'Declined';
    public static final STring OFFER_OFFFERED = 'Offered';
    public static final String OFFER_CANCELLED = 'Cancelled';

    //Notice message when resource accepts or declines offer
    public static final String RES_ACCEPT_OFFER = 'Thanks for accepting the Job. You are now allocated to this Job. Please click Close.';
    public static final String RES_DECLINE_OFFER = 'Thanks for declining the Job. You will be removed from the Job Offer list. Please select Close.';
    public static final String RES_NOT_AVAI = 'Thanks for checking this Job. Unfortunately, you have been allocated to another Job, the offer will be removed from your list.';
    public static final String OFFER_NOT_AVAI = 'Thanks for accepting this offer. Unfortunately, it has been allocated or is no longer available. The offer will be removed from your list.';

    // Service charge per type
    public static final String SERVICE_CHARGE_PER_BED = 'Bed';
    public static final String SERVICE_CHARGE_PER_CLIENT = 'Client';
    
    //Async Job Action
    public static final String CLONE_JOB_RELATED_LIST = 'Clone Job Related List';

}
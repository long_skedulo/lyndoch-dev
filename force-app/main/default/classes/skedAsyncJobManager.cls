public virtual class skedAsyncJobManager implements skedIAsyncJobManager{
    public static Map<String, String> mapJobToken = new Map<String,String>();

    public String PENDING_STATUS        = 'Pending';
    public String SCHEDULED_STATUS      = 'Scheduled';
    public String ERROR_STATUS          = 'Error';
    public String COMPLETED_STATUS      = 'Completed';

    /**
    * Register a job to the job manager
    */
    public virtual void register(String action, Object param){
        Map<String,String> mapAction2JobManager = getActionMap();
        skedAsyncJob job = new skedAsyncJob(action, param);
        if(!mapAction2JobManager.containsKey(action)){
            System.debug(LoggingLevel.ERROR,'Action not support: ' + action);
            this.startJob(job);
        }
        skedIAsyncJobManager jobManager = (skedIAsyncJobManager)Type.forName(mapAction2JobManager.get(action)).newInstance();
        jobManager.startJob( job );
    }
    
    /**
    * Returns a map of actions and the actual job manager classes handling the actions
    */
    public virtual Map<String,String> getActionMap(){
        return new Map<String,String>{};
    }

    /**
    * Start a job
    */
    public virtual void startJob(skedAsyncJob job){
        job.jobManager = this;
    }

    /**
    * Pre-processing
    */
    public virtual void preExec(skedAsyncJob job){}

    /**
    *Handling the execution of synchronous jobs, Schedulable jobs and serial jobs
    */
    public virtual void executeJob(skedAsyncJob job){}

    /**
    * Post-processing
    */
    public virtual void postExec(skedAsyncJob job){
        if(job.isSerialJob && job.isCompleted){
            clearToken(job);
            updateQueue(job);
            //Reschedule the serial job
            job.param = null;
            startJob(job);
        }
    }

    /**
    * Handle Batchable.start()
    */
    public virtual List<sObject> startBatch(skedAsyncJob job){
        return new List<sObject>();
    }

    /**
    * Handle Batchable.start()
    */
    public virtual void executeBatch(skedAsyncJob job){
        
    }

    /**
    * Handle Batchable.finish()
    */
    public virtual void finishBatch(skedAsyncJob job){
        postExec(job);
    }

    //======= EXCUTION  METHODS =======
    
    //Handling the execution of synchronous jobs, Schedulable jobs and serial jobs
    public virtual void execute(skedAsyncJob job){
        job.execute();
    }

    //======= STARTING JOB  METHODS =======

    // Execute the job immediately
    public virtual void startNow(skedAsyncJob job){
        executeJob(job);
    }

    // Start a batch job with default batch size (200)
    public virtual void startBatchJob(skedAsyncJob job){
        job.isBatchable = true;
        Database.executeBatch(job);
    }

    // Start a batch job with a pre-defined batch size
    public virtual void startBatchJob(skedAsyncJob job, Integer batchSize){
        Database.executeBatch(job, Test.isRunningTest()?200:batchSize);
    }

    // Start a Schedulable job with a pre-defined delay in second
    public virtual void startAfter(skedAsyncJob job, Integer seconds){
        System.schedule(job.action + ' action by ' + UserInfo.getUserName() + ' ' + generateRandomString(10), getSchedulingString(seconds), job);
    }

    // Start a serial job 
    public virtual void startSerialJobAfter(skedAsyncJob job, Integer seconds){
        job.isSerialJob = true;
        if(job.param != null) pushToQueue(job);
        //Request a token from the job manager
        String token = requestToken(job);
        if(String.isBlank(token)) {//Another job of this type is running in the same transaction
            return;
        }
        //token granted
        job.token = token;

        if( hasScheduledJob(job.action) ) {//Another job of this type is running in another transaction
            clearToken(job);
            return;
        }else{
            if(hasPeningJob(job)){
                startAfter(job, seconds);
            }
        }
    }

    //======= MANAGING JOB STACK METHODS =======

    // Request a token for this job type
    public virtual String requestToken(skedAsyncJob job){
        String token = '';
        if(!mapJobToken.containsKey(job.action)){//No job of this type is running
            token = generateRandomString(10);
            mapJobToken.put( job.action, token );
        }
        return token;
    }

    // Clear the token
     public virtual void clearToken(skedAsyncJob job){
        if(mapJobToken.containsKey(job.action) && mapJobToken.get(job.action) == job.token){
            mapJobToken.remove( job.action );
        }
    }

    // Create a Async Job record with Pending status
    public virtual void pushToQueue(skedAsyncJob job){
        if(job.param != null){
            
            sked__Async_Job__c skedAsyncJob =  new sked__Async_Job__c(
                                                sked__Param__c      = JSON.serialize(job.param),
                                                sked__Status__c     = PENDING_STATUS,
                                                sked__Type__c       = job.action
                                            );
            insert skedAsyncJob;
        }
    }

    // Delete the Async record if successful
    // Otherwise, log an erro
    public void updateQueue(skedAsyncJob job){
        if(job.jobId != null){//Delete the Async record
            if(job.exceptions ==null || job.exceptions.isEmpty()){
                Database.delete( new sked__Async_Job__c(Id = job.jobId) );
            }else{//Log the error
                String msg = job.exceptions.get(0).getMessage() + '\n' + job.exceptions.get(0).getStackTraceString();
                Database.update( new sked__Async_Job__c(Id = job.jobId, sked__Status__c = ERROR_STATUS, sked__Message__c = msg) );
            }
        }
    }
    
    // Check if there's any Pending Async Job record of the same action
    public virtual boolean hasPeningJob(skedAsyncJob job){
        List<sked__Async_Job__c> skedAsyncJobs = [
            SELECT Id, sked__Param__c
            FROM sked__Async_Job__c
            WHERE sked__Status__c  = :PENDING_STATUS AND sked__Type__c = :job.action
            ORDER BY CreatedDate ASC LIMIT 1
        ];
        if( skedAsyncJobs.isEmpty() ) return false;
        //Schedule the job
        sked__Async_Job__c skedAsyncJob = skedAsyncJobs.get(0);
        skedAsyncJob.sked__Status__c  = SCHEDULED_STATUS;
        update skedAsyncJob;
        job.jobId   = skedAsyncJob.id;
        //Convert Param__c to an Object
        job.param = ((Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(skedAsyncJob))).get('param');

        return true;
    }

    // Check if there's any job of the same type (action) running in another transaction
    public virtual boolean hasScheduledJob(String action){
        Integer jobCount = [
            SELECT count()
            FROM sked__Async_Job__c
            WHERE sked__Status__c  = :SCHEDULED_STATUS AND sked__Type__c = :action
        ];
        return jobCount > 0;
    }
    
    // Generate a random string
    public String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
           Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
           randStr += chars.substring(idx, idx+1);
        }
        return randStr;
    }

    // Get scheduling time string, a CRON expression
    public static String getSchedulingString(Integer sec){
        return getSchedulingString(System.now(), sec);
    }

    // Get scheduling time string, a CRON expression
    public static String getSchedulingString(DateTime refTime, Integer sec){
        dateTime dt= refTime.addSeconds(sec);
        String Csec, Cmin, Chr, Cday, Cmonth, CYear;
        Csec    = String.valueof(dt.second());
        Cmin    = String.valueof(dt.minute());
        Chr     = String.valueof(dt.hour());
        Cday    = String.valueof(dt.day());
        Cmonth  = String.valueof(dt.month());
        CYear   = String.valueof(dt.Year());

        // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        String SchTimer = Csec + ' ' + Cmin + ' ' + Chr + ' ' + Cday + ' ' + Cmonth + ' ? ' + CYear;
        return SchTimer;
    }
}
public interface skedIAsyncJobManager {
    /**
    * Register a job
    */
    void register(String action, Object param);

    /**
    * Handle job done
    */
    void startJob(skedAsyncJob job);

    /**
    * Pre-processing job
    */
    void preExec(skedAsyncJob job);

    /**
    * Execute Job
    */
    void executeJob(skedAsyncJob job);

    /**
    * Post-processing job
    */
    void postExec(skedAsyncJob job);

    /**
    * Handle Batchable.start()
    */
    List<sObject> startBatch(skedAsyncJob job);

    /**
    * Handle Batchable.start()
    */
    void executeBatch(skedAsyncJob job);

    /**
    * Handle Batchable.finish()
    */
    void finishBatch(skedAsyncJob job);
}
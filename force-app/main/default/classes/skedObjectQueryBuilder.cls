global virtual class skedObjectQueryBuilder {

    // object field name
    protected Set<String> objectFields;
    protected String objectName;
    // query for related childs. Key is the related child API name
    protected Map<String, skedObjectQueryBuilder> relatedChildQueries;

    global String whereClause {get;set;}
    // Order By, Litmit..
    global String extraClause {get;set;}

    // Constructor
    global skedObjectQueryBuilder() {
        this.objectFields = new Set<String>();
        this.relatedChildQueries = new Map<String, skedObjectQueryBuilder>();
    }

    global skedObjectQueryBuilder(String objectName) {
        this();
        this.objectName = objectName;
    }

    global virtual String getObjectName() {
        return this.objectName;
    }

    

    global virtual void addFields(Set<String> fields) {
        objectFields.addAll(fields);
    }

    global virtual void addChildQuery(String childAPIName, skedObjectQueryBuilder childQuery) {
        relatedChildQueries.put(childAPIName, childQuery);
    }

    

    global virtual String getQuery() {
        String query = 'SELECT ' + String.join(new List<String>(objectFields), ',');

        if (relatedChildQueries != null & !relatedChildQueries.isEmpty()) {
            for (skedObjectQueryBuilder childQuery : relatedChildQueries.values()) {
                query += ' , ( ' + childQuery.getQuery() + ' ) ';
            }
        }

        query += ' FROM ' + this.objectName + ' ';
        query = addToQuery(query, whereClause);
        query = addToQuery(query, extraClause);

        return query;
    }

    

    global virtual String addToQuery(String query, String extra) {
        if (String.isNotBlank(extra)) {
            return query + ' ' + extra;
        }
        return query;
    }
}
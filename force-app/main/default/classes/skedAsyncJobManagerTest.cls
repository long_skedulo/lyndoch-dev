@isTest
public class skedAsyncJobManagerTest {
    @testSetup
    static void initData() {
        Account account = skedTestDataFactory.createAccounts('Test Account', 1).get(0);
        insert account;
        
        skedAsyncJobManager jobManager = new skedAsyncJobManager();
        
        skedAsyncJob asyncJob = new skedAsyncJob('Test Action', new Set<Id>{account.Id});
        asyncJob.jobManager = jobManager;
        
        jobManager.startNow(asyncJob);
        jobManager.execute(asyncJob);
        jobManager.startBatchJob(asyncJob);
        jobManager.startBatchJob(asyncJob, 200);
        jobManager.startAfter(asyncJob, 10);
        //Test serial job
        Integer asyncJobCount = [Select count() from sked__Async_Job__c];
        System.assertEquals(asyncJobCount, 0);

        jobManager.startSerialJobAfter(asyncJob, 10);

        asyncJobCount = [Select count() from sked__Async_Job__c];
        System.assertEquals(asyncJobCount, 1);
    }
    
    @isTest
    static void testLyndochJobManager() {
        skedTestDataFactory.setupCustomSettings();
        Account account = skedTestDataFactory.createAccounts('Test Account', 1).get(0);
        insert account;		        
        
        sked__Region__c region = skedTestDataFactory.createRegion('Sydney','Australia/Sydney');
        insert region;
		
        Contact client = skedTestDataFactory.createContact(account.Id, 'Test Client');
        insert client;
        
        sked__Location__c location = skedTestDataFactory.createLocation('Test Location', account.Id, region.Id);
        location.sked__GeoLocation__Latitude__s = -27.457730;
        location.sked__GeoLocation__Longitude__s = 153.037080;
        insert location;
		
        sked__Job__c job = skedTestDataFactory.createJobs(account.Id, region.Id, null, 2).get(0);
        job.sked__Contact__c = client.Id;
        job.sked__Job_Status__c = 'Pending Allocation';
        job.sked__GeoLocation__Latitude__s = location.sked__GeoLocation__Latitude__s;
        job.sked__GeoLocation__Longitude__s = location.sked__GeoLocation__Longitude__s;
        insert job;

        sked__Resource__c resource = skedTestDataFactory.createResource('Test', UserInfo.getUserId(), region.Id);
        insert resource;

        sked__Job_Allocation__c ja = skedTestDataFactory.allocateJob(job.Id, resource.Id);
        ja.sked__Status__c = 'Confirmed';
        insert ja;

        Test.startTest();

        job.sked__Job_Status__c = 'Complete';
        update job;
		skedLyndochJobManager.newInstance().register(skedConstants.CLONE_JOB_RELATED_LIST, new Set<Id>{job.Id});
        System.assertNotEquals(null, job.Id);
        Test.stopTest();
    }

}
public abstract class skedJobRelatedListCloning {
    public static final String PRODUCTS = 'Products';

    protected RelatedListParameter parameter;
    protected List<sObject> newProducts;

    protected abstract Id getReferenceKey(sked__Job__c job);

    public skedJobRelatedListCloning() {
        this.newProducts = new List<sObject>();
    }

    public virtual void cloneRelatedList(List<sked__Job__c> inputJobs, Map<Id, sked__Job__c> mapReferencedJob) {
        system.debug('parameter==' + parameter);
        if (parameter == null) return;
        system.debug('parameter==' + parameter);
        // Clone related list, except job service items, because it has reference to group attendee
        for (sked__Job__c job : inputJobs) {
            Id referenceKey = getReferenceKey(job);
            sked__Job__c referencedJob = mapReferencedJob.get(referenceKey);
            if (referencedJob != null) {
                if (parameter.cloneJobProducts) {
                    cloneProducts(job, referencedJob);
                }
            }
        }

        insertRecords(newProducts);       
    }

    protected void insertRecords(List<sObject> objects) {
        if (objects != null && !objects.isEmpty()) {
            insert objects;
        }
    }
    
    protected virtual void cloneProducts(sked__Job__c job, sked__Job__c referencedJob) {
        sked__Job_Product__c newProduct;

        if (referencedJob.sked__Job_Products__r != null && !referencedJob.sked__Job_Products__r.isEmpty()) {
            for (sked__Job_Product__c jobProduct : referencedJob.sked__Job_Products__r) {
                newProduct = cloneJobProduct(job, jobProduct);
                newProducts.add(newProduct);
            }
        }
    }

    protected virtual sked__Job_Product__c cloneJobProduct(sked__Job__c job, sked__Job_Product__c originalProduct) {
        sked__Job_Product__c jobProduct = new sked__Job_Product__c(
            sked__Job__c = job.Id,
            sked__Product__c = originalProduct.sked__Product__c,
            sked_Description__c = originalProduct.sked_Description__c,
            sked_Price__c = originalProduct.sked_Price__c,
            sked_Unit_Price__c = originalProduct.sked_Unit_Price__c,
            sked__Qty__c = originalProduct.sked__Qty__c,
            sked_Day_Of_Week__c = originalProduct.sked_Day_Of_Week__c
        );
        return jobProduct;
    }  

    protected virtual skedObjectQueryBuilder getJobQueryBuilder() {
        skedObjectQueryBuilder jobQueryBuilder = new skedObjectQueryBuilder('sked__Job__c');
        jobQueryBuilder.addFields(new Set<String>{
            'Id', 'Name', 'sked__Recurring_Schedule__c'
        });

        // Add child queries
        if (this.parameter.cloneJobProducts) {
            addJobProductQuery(jobQueryBuilder);
        }
        return jobQueryBuilder;
    }

    protected virtual void addJobProductQuery(skedObjectQueryBuilder jobQueryBuilder) {
        String objName = 'sked__Job_Products__r';
        Set<String> fieldNames = new Set<String>{
            'Id', 'Name', 'sked__Job__c', 'sked__Product__c', 'sked__Qty__c', 'sked_Description__c', 'sked_Day_Of_Week__c', 'sked_Price__c', 'sked_Unit_Price__c'
        };

        skedObjectQueryBuilder cJobProductBuilder = newChildQueryBuilder(objName, fieldNames);
        jobQueryBuilder.addChildQuery(cJobProductBuilder.getObjectName(), cJobProductBuilder);
    }    

    protected virtual skedObjectQueryBuilder newChildQueryBuilder(String objName, Set<String> fieldNames) {
        skedObjectQueryBuilder builder = new skedObjectQueryBuilder(objName);
        builder.addFields(fieldNames);
        return builder;
    }

    protected String getKeyFrom(String keyOne, String keyTwo) {
        return keyOne + '_' + keyTwo;
    }

    public class RelatedListParameter {
        public Boolean cloneJobProducts;       

        public RelatedListParameter() {
            this('');
        }

        public RelatedListParameter(String relatedListSetting) {            
            this.cloneJobProducts = relatedListSetting.containsIgnoreCase(PRODUCTS);
        }
    }
}
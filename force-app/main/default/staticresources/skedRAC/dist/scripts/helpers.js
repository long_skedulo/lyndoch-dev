(function(window, moment) {

  window.helpers = {
    timeNumFormat: function (num, outFormat) {
      num = num%2400
      var s = '' + num, count = 4 - s.length
      for (var i=0; i<count; i++) {
        s = '0' + s
      }
      var result = moment(s, "Hm").format(outFormat) // hha 
      // console.log(s, result)
      return result
    },

    getTimeRange: function (item) {
      var count = Math.floor(60/item.step)
      var arr = [0], diff = 60 / count
      for (var i=0; i<count-1; i++) {
        arr.push(diff)
      }
      arr.push(100-diff*(count-1))
      // console.log(count, diff, arr)

      var start = item.availableStart, end = item.availableEnd, val = start, i = 0, results = []
      // console.log(start, end, diff)
      while (val < end) {
        val += arr[i]
        // console.log(val, i, timeNumFormat(val))
        results.push({ id: val, label: this.timeNumFormat(val, 'hh:mm a') })
        i += 1
        if (i > count) { i = 1}
      }
      return results
    }
  }

})(window, moment);
@isTest
public class skedJobProductServiceTest {
	@testSetup
    static void initData() {
        skedTestDataFactory.setupCustomSettings();
        Account account = skedTestDataFactory.createAccounts('Test Account', 1).get(0);
        insert account;		        
        
        sked__Region__c region = skedTestDataFactory.createRegion('Sydney', UserInfo.getTimeZone().getID());
        insert region;
		
        Contact client = skedTestDataFactory.createContact(account.Id, 'Test Client');
        insert client;
        
        sked__Location__c location = skedTestDataFactory.createLocation('Test Location', account.Id, region.Id);
        location.sked__GeoLocation__Latitude__s = -27.457730;
        location.sked__GeoLocation__Longitude__s = 153.037080;
        insert location;
        
        Product2 product = skedTestDataFactory.createProduct('Test Product', 'P1');
        insert product;
        
        sked__Recurring_Schedule__c schedule = skedTestDataFactory.createRecurringSchedule();
		
        List<sked__Job__c> jobs = skedTestDataFactory.createJobs(account.Id, region.Id, null, 2);
        for ( sked__Job__c job : jobs ) {
            job.sked__Recurring_Schedule__c = schedule.Id;
            job.sked__Contact__c = client.Id;
            job.sked__Job_Status__c = 'Pending Allocation';
            job.sked__GeoLocation__Latitude__s = location.sked__GeoLocation__Latitude__s;
            job.sked__GeoLocation__Longitude__s = location.sked__GeoLocation__Longitude__s;
        }
        insert jobs;
        sked__Job__c job = jobs.get(0);                
		
        /*sked__Job_Product__c jobProd = skedTestDataFactory.createJobProduct(job.Id, product.Id, 1);
        insert jobProd;*/

        sked__Resource__c resource = skedTestDataFactory.createResource('Test', UserInfo.getUserId(), region.Id);
        insert resource;

        sked__Job_Allocation__c ja = skedTestDataFactory.allocateJob(job.Id, resource.Id);
        ja.sked__Status__c = 'Confirmed';
        insert ja;
    }
    
    @isTest static void createJobProduct() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RequestBody requestBody = new RequestBody();
        requestBody.apiName = 'createJobProduct';
        skedJobProductService.JobProductModel jobProdModel = new skedJobProductService.JobProductModel();
        jobProdModel.jobId = [SELECT Id FROM sked__Job__c LIMIT 1].Id;
        jobProdModel.description = 'This is testing';
        jobProdModel.productId = [SELECT Id FROM Product2 LIMIT 1].Id;
        jobProdModel.quantity = 1;
        jobProdModel.weekday = 'Mon';
        jobProdModel.price = 100;
        skedJobProductService.ServiceRequestModel requestModel = new skedJobProductService.ServiceRequestModel();
        requestModel.applyForAll = TRUE;
        requestModel.product = jobProdModel;
        requestBody.payload = requestModel;
        req.requestURI = '/services/apexrest/skedJobProductService/'; 
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf( JSON.serialize(requestBody) );
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        skedJobProductService.handlePost();        
        Test.stopTest();
        String jsonData = getJsonData(res);      
        System.assertEquals(2, [SELECT count() FROM sked__Job_Product__c]);
    }
    
    @isTest static void updateJobProduct() {
        Product2 product = [SELECT Id FROM Product2 LIMIT 1];
        List<sked__Job_Product__c> jobProducts = new List<sked__Job_Product__c>();
        for ( sked__job__c job : [SELECT Id FROM sked__job__c ORDER BY sked__Start__c] ) {
            jobProducts.add(skedTestDataFactory.createJobProduct(job.Id, product.Id, 1));
        }        
        insert jobProducts;
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RequestBody requestBody = new RequestBody();
        requestBody.apiName = 'updateJobProduct';
        skedJobProductService.JobProductModel jobProdModel = new skedJobProductService.JobProductModel();
		jobProdModel.id = jobProducts.get(0).Id;
        jobProdModel.jobId = [SELECT Id FROM sked__Job__c ORDER BY sked__Start__c LIMIT 1].Id;
        jobProdModel.description = 'This is testing';
        jobProdModel.productId = product.Id;
        jobProdModel.quantity = 100;
        jobProdModel.weekday = 'Mon';
        jobProdModel.price = 100;
        skedJobProductService.ServiceRequestModel requestModel = new skedJobProductService.ServiceRequestModel();
        requestModel.applyForAll = TRUE;
        requestModel.product = jobProdModel;
        requestBody.payload = requestModel;
        req.requestURI = '/services/apexrest/skedJobProductService/'; 
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf( JSON.serialize(requestBody) );
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        skedJobProductService.handlePost();        
        Test.stopTest();
        String jsonData = getJsonData(res);  
        System.assertEquals(100, [SELECT sked__Qty__c FROM sked__Job_Product__c WHERE sked__Job__c = :jobProdModel.jobId].sked__Qty__c);
    }
    
    @isTest static void deleteJobProduct() {
        Product2 product = [SELECT Id FROM Product2 LIMIT 1];
        List<sked__Job_Product__c> jobProducts = new List<sked__Job_Product__c>();
        for ( sked__job__c job : [SELECT Id FROM sked__job__c ORDER BY sked__Start__c] ) {
            jobProducts.add(skedTestDataFactory.createJobProduct(job.Id, product.Id, 1));
        }        
        insert jobProducts;
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        RequestBody requestBody = new RequestBody();
        requestBody.apiName = 'deleteJobProduct';
        skedJobProductService.JobProductModel jobProdModel = new skedJobProductService.JobProductModel();
		jobProdModel.id = jobProducts.get(0).Id;
        jobProdModel.jobId = [SELECT Id FROM sked__Job__c ORDER BY sked__Start__c LIMIT 1].Id;
        jobProdModel.description = 'This is testing';
        jobProdModel.productId = product.Id;
        jobProdModel.quantity = 100;
        jobProdModel.weekday = 'Mon';
        jobProdModel.price = 100;
        skedJobProductService.ServiceRequestModel requestModel = new skedJobProductService.ServiceRequestModel();
        requestModel.applyForAll = TRUE;
        requestModel.product = jobProdModel;
        requestBody.payload = requestModel;
        req.requestURI = '/services/apexrest/skedJobProductService/'; 
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf( JSON.serialize(requestBody) );
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        skedJobProductService.handlePost();        
        Test.stopTest();
        String jsonData = getJsonData(res);   
        System.assertEquals(0, [SELECT count() FROM sked__Job_Product__c]);
    }
    
    public class RequestBody {
        public String apiName;
        public Object payload;
        public RequestBody(){}
    }
    
    static String getJsonData(RestResponse res) {
        System.debug('res.responseBody.toString()#' + res.responseBody.toString());
        Map<String, Object> untypedObjectMap = (Map<String, Object>)JSON.deserializeUntyped(res.responseBody.toString());
        String jsonData = JSON.serialize(untypedObjectMap.get('data'));
        return jsonData;
    }
}
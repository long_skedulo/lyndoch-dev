trigger skedAvailabilityTrigger on sked__Availability__c (after insert, after update) {
    if (trigger.isAfter) {
        if (trigger.isInsert) {
            skedAvailabilityHandler.onAfterInsert(trigger.new);
        }
        else if (trigger.isUpdate) {
            skedAvailabilityHandler.onAfterUpdate(trigger.new, trigger.oldMap);
        }
    }
}
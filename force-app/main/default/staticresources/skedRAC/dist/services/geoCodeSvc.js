(function(angular) {
  
  angular.module('svc.geoCode', [])

    .constant('geoCodeAPIKey', 'AIzaSyA83ZOeD6Gw3eRxuSlxOE-dPA3l_iLRVkY')
    .factory('geoCodeSvc', ['$q', '$http', 'geoCodeAPIKey', function($q, $http, geoCodeAPIKey) {
      return {
        get: function(query, country) {
          var deferred = $q.defer()
          if (query.toLowerCase().indexOf(country) === -1) {
            if (country) query += ' ' + country
          }
          var url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + query +
            '&key=' + geoCodeAPIKey + ' &sensor=true'
          $http.get(url)
            .then(function(response) {
              var results = _(response.data.results)
              .map(function(item, index) {
                return {
                  label: item.formatted_address,
                  data: item
                }
              }).value()
              deferred.resolve(results)
            })
          return deferred.promise
        }
      }
    }])

})(angular);
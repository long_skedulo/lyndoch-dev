global virtual class skedConfigs {

    private static Map<String, skedRACConfigs__c> skedConfigsInstance = null;

    private static final String CLIENT_JOB_CONFLICT = 'Job Conflict';
    private static final String CLIENT_AVAILABILITY = 'Availability';
    /**
    *@description Singleton to get skedConfigs custom setting
    *
    */
    global static Map<String, skedRACConfigs__c> getskedConfigsCustomSettings() {
        if(skedConfigsInstance == null) {
            skedConfigsInstance = skedRACConfigs__c.getAll();
        }
        return skedConfigsInstance;
    }

    global static String GOOGLE_API_KEY{
        get{
            if(GOOGLE_API_KEY == null){
                if(getskedConfigsCustomSettings().containsKey('Global_GoogleAPIKEY')){
                    GOOGLE_API_KEY     = getskedConfigsCustomSettings().get('Global_GoogleAPIKEY').Value__c;
                }else GOOGLE_API_KEY   = '';
            }
            return GOOGLE_API_KEY;
        }
    }

    global static String UNAVAI_NOTIFICATION_EMAIL_SUBJECT{
        get{
            if(UNAVAI_NOTIFICATION_EMAIL_SUBJECT == null){
                if(getskedConfigsCustomSettings().containsKey('Unavailability_Email_Subject')){
                    UNAVAI_NOTIFICATION_EMAIL_SUBJECT     = getskedConfigsCustomSettings().get('Unavailability_Email_Subject').Value__c;
                }else UNAVAI_NOTIFICATION_EMAIL_SUBJECT   = '';
            }
            return UNAVAI_NOTIFICATION_EMAIL_SUBJECT;
        }
    }

    global static String UNAVAI_NOTIFICATION_EMAIL_TEMPLATE{
        get{
            if(UNAVAI_NOTIFICATION_EMAIL_TEMPLATE == null){
                if(getskedConfigsCustomSettings().containsKey('Unavailability_Email_Template')){
                    UNAVAI_NOTIFICATION_EMAIL_TEMPLATE     = getskedConfigsCustomSettings().get('Unavailability_Email_Template').Value__c;
                }else UNAVAI_NOTIFICATION_EMAIL_TEMPLATE   = '';
            }
            return UNAVAI_NOTIFICATION_EMAIL_TEMPLATE;
        }
    }

    global static String AVAILABILITY_REQUEST_LINK{
        get{
            if(AVAILABILITY_REQUEST_LINK == null){
                if(getskedConfigsCustomSettings().containsKey('Availability_Request_Link')){
                    AVAILABILITY_REQUEST_LINK     = getskedConfigsCustomSettings().get('Availability_Request_Link').Value__c;
                }else AVAILABILITY_REQUEST_LINK   = '';
            }
            return AVAILABILITY_REQUEST_LINK;
        }
    }

    global static String AVAILABILITY_NOTIFY_CHATTER_GROUP_NAME{
        get{
            if(AVAILABILITY_NOTIFY_CHATTER_GROUP_NAME == null){
                if(getskedConfigsCustomSettings().containsKey('Availability_Notify_Chatter_Group_Name')){
                    AVAILABILITY_NOTIFY_CHATTER_GROUP_NAME     = getskedConfigsCustomSettings().get('Availability_Notify_Chatter_Group_Name').Value__c;
                }else AVAILABILITY_NOTIFY_CHATTER_GROUP_NAME   = '';
            }
            return AVAILABILITY_NOTIFY_CHATTER_GROUP_NAME;
        }
    }
    
    /**
    * @description
    * Delay time to run batch job to clone job related lists when creating a recurring job
    * @return Integer: unit is second
    */
    public static Integer RECURRING_JOB_REPLICATION_DELAY_TIME {
        get{
            if(skedConfigs.getskedConfigsCustomSettings().containsKey('Recurring_Job_Replication_Delay_Time')){
                RECURRING_JOB_REPLICATION_DELAY_TIME = Integer.valueOf(getskedConfigsCustomSettings().get('Recurring_Job_Replication_Delay_Time').Value__c);
            }else RECURRING_JOB_REPLICATION_DELAY_TIME   = 120;
            return RECURRING_JOB_REPLICATION_DELAY_TIME;
        }
        set;
    }
    
    public static String CLONING_JOB_RELATED_LIST {
        get{
            if(skedConfigs.getskedConfigsCustomSettings().containsKey('Cloning_Job_Related_List')){
                CLONING_JOB_RELATED_LIST     = skedConfigs.getskedConfigsCustomSettings().get('Cloning_Job_Related_List').Value__c;
            }else CLONING_JOB_RELATED_LIST   = '';
            return CLONING_JOB_RELATED_LIST;
        }
        set;
    }
    
    public static boolean ENABLE_LOG{
        get{
            if(getskedConfigsCustomSettings().containsKey('Enable_Log ')){
                ENABLE_LOG     = Boolean.valueOf(getskedConfigsCustomSettings().get('Enable_Log ').Value__c);
            }else ENABLE_LOG   = false;
            return ENABLE_LOG;
        }
        set;
    }

    global static String RESOURCE_DISTANCE_FIRST    = 'First';
    global static String RESOURCE_DISTANCE_COMBINED    = 'Combined';
    global static String DISTANCE_TRAVELLED_SPLIT2    = 'Split';
    global static String DISTANCE_TRAVELLED_ALL    = 'All';


}
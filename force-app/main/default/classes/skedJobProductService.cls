@RestResource(urlMapping='/skedJobProductService')
global class skedJobProductService {  
    @HttpPost
    global static void handlePost ()   {
        Restrequest req = RestContext.request;
        Restresponse res = RestContext.response;
        skedResponse response = handleRequest(req, res);
        String resBody = setRespondSuccess(res, 200, response);        
        res.responseBody = Blob.valueOf(resBody);
        if ( skedConfigs.ENABLE_LOG )
            log(req);
    }

    static skedResponse handleRequest(Restrequest req, Restresponse res) {
        skedResponse response = new skedResponse();        
        try{
            Map<String, Object> untypedObjectMap = getUntypedObjectMap(req);  
            String apiName = (String)untypedObjectMap.get('apiName');
            String payload = JSON.serialize(untypedObjectMap.get('payload'));
            if ( apiName != NULL ) {
                if ( apiName.equalsIgnoreCase('updateJobProduct')) {
                    ServiceRequestModel request = (ServiceRequestModel)JSON.deserialize(payload, ServiceRequestModel.class);
                    updateJobProduct(request);                    
                }
                else if ( apiName.equalsIgnoreCase('deleteJobProduct') ) {
                    ServiceRequestModel request = (ServiceRequestModel)JSON.deserialize(payload, ServiceRequestModel.class);
                    deleteJobProduct(request);
                }
                else if ( apiName.equalsIgnoreCase('createJobProduct') ) {
                    ServiceRequestModel request = (ServiceRequestModel)JSON.deserialize(payload, ServiceRequestModel.class);
                    createJobProduct(request);
                }
                response.success = true;
            }
            else {
                response.success = true;
                response.message = 'Cannot execute function. API not found';                
            }
        }
        catch(Exception ex){   
            setRespondFail(res, 500, ex.getMessage() + ex.getLineNumber() + ex.getStackTraceString());         
            System.Debug('####Exception:###: ' + ex.getMessage() + '##:##' + ex.getStackTraceString());
        }      
        return response;  
    }
    
    static void updateJobProduct(ServiceRequestModel request) {
        JobProductModel jp = request.product;
        sked__Job_Product__c skedJobProduct = populateJobProductInfo(jp);
        update skedJobProduct;
        if ( request.applyForAll ) {
            List<sked__Job_Product__c> jobProductToUpdate = getJobProductToUpdate(jp);
            if ( !jobProductToUpdate.isEmpty() ) {
                update jobProductToUpdate;
            }
        }        
    } 
    
    static void createJobProduct(ServiceRequestModel request) {
        JobProductModel jp = request.product;
        sked__Job_Product__c skedJobProduct = populateJobProductInfo(jp);
        insert skedJobProduct;
        if ( request.applyForAll ) {
            List<sked__Job_Product__c> jobProductToCreate = getJobProductToCreate(jp);
            if ( !jobProductToCreate.isEmpty() ) {
                insert jobProductToCreate;
            }
        }        
    }
    
    static void deleteJobProduct(ServiceRequestModel request) {
        JobProductModel jp = request.product;
        sked__Job_Product__c skedJobProduct = populateJobProductInfo(jp);
        delete skedJobProduct;
        if ( request.applyForAll ) {
        	List<sked__Job_Product__c> jobProductToDelete = getJobProductToDelete(jp);
            if ( !jobProductToDelete.isEmpty() ) {
                delete jobProductToDelete;
            }
        }      
    }
    
    static sked__Job_Product__c populateJobProductInfo(JobProductModel jpModel) {
        sked__Job_Product__c skedJobProd = new sked__Job_Product__c(
            	Id = jpModel.id,
                sked__Product__c = jpModel.productId,
                sked_Description__c = jpModel.description,
                sked__Qty__c  = jpModel.quantity,
                sked_Day_Of_Week__c  = jpModel.weekday,
	            sked_Package_Name__c = jpModel.packageName,
            	sked_Package__c  = jpModel.packageId,
            	sked_Unit_Price__c  = jpModel.unitPrice,
            	sked_Price__c = jpModel.price
            );
        if ( jpModel.id == NULL ) skedJobProd.sked__Job__c = jpModel.jobId;
        return skedJobProd;
    }
    
    static Map<String, Object> getUntypedObjectMap(Restrequest req) {
        Map<String, Object> untypedObjectMap = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.toString());
        return untypedObjectMap;
    }
    
    static String setRespondFail(Restresponse res, Integer code, String msg) {
        res.statusCode = code;
        return '{"success": false, "msg": "' + msg + '"}';
    }
    
    static String setRespondSuccess(Restresponse res, Integer code, skedResponse response) {
        res.statusCode = code;
        return JSON.serialize(response, true);
    }
    
    static void log(Restrequest req){
        if (req != NULL) {
            String message = 'Headers: ' + String.valueOf(req.headers) + '\n'
                            + 'HttpMethod: ' + req.httpMethod + '\n'
                            + 'Params: ' + String.valueOf(req.params) + '\n'
                            + 'RemoteAddress: ' + req.remoteAddress + '\n'
                            + 'RequestURI: ' + req.requestURI  + '\n'
                            + 'ResourcePath: ' + req.resourcePath + '\n'
                            + 'RequestBody:' + '\n' + req.requestBody.toString();
            sked_Custom_Log__c log = new sked_Custom_Log__c(
                sked_Message__c = message
            );
            insert log;
            System.debug('message==' + message);
        }
    }
    
    static List<sked__Job_Product__c> getJobProductToUpdate(JobProductModel jpModel) {        
		List<sked__Job__c> skedJobs = queryRecurringJobWithProduct(jpModel);
        System.debug('skedJobs#' + skedJobs);
        List<sked__Job_Product__c> jobProductToUpdate = new List<sked__Job_Product__c>();
        Map<Id, Map<Id, List<sked__Job_Product__c>>> job_Product_JobProduct_Map = buildJobToProductMap(skedJobs);
        for ( sked__Job__c job : skedJobs ) {
            Map<Id, List<sked__Job_Product__c>> product_JobProduct_Map = job_Product_JobProduct_Map.get(job.Id);   
            jpModel.jobId = job.Id;
            System.debug('jpModel.productId#' + jpModel.productId);
            System.debug('job_JobProduct_Map#' + product_JobProduct_Map.keySet());
            if ( product_JobProduct_Map.containsKey(jpModel.productId) ) {
                List<sked__Job_Product__c> jobProducts = product_JobProduct_Map.get(jpModel.productId);
                System.debug('jobProducts#' + jobProducts);
                for ( sked__Job_Product__c jobProduct : jobProducts ) {
                    jpModel.id = jobProduct.Id;
                    sked__Job_Product__c updatedJobProduct = populateJobProductInfo(jpModel); 
                    jobProductToUpdate.add(updatedJobProduct);
                }                
            }
        }
        
        return jobProductToUpdate;                    
    }
    
    static List<sked__Job_Product__c> getJobProductToCreate(JobProductModel jpModel) {    
    	List<sked__Job__c> skedJobs = queryRecurringJobWithProduct(jpModel);
        System.debug('skedJobs#' + skedJobs);
        List<sked__Job_Product__c> jobProductToCreate = new List<sked__Job_Product__c>();
        Map<Id, Map<Id, List<sked__Job_Product__c>>> job_Product_JobProduct_Map = buildJobToProductMap(skedJobs);
        for ( sked__Job__c job : skedJobs ) {
            Map<Id, List<sked__Job_Product__c>> job_JobProduct_Map = job_Product_JobProduct_Map.get(job.Id);   
            jpModel.jobId = job.Id;
            if ( !job_JobProduct_Map.containsKey(jpModel.productId) ) {
                sked__Job_Product__c jobProduct = populateJobProductInfo(jpModel); 
                jobProductToCreate.add(jobProduct);
            }
        }
        return jobProductToCreate;
    }
    
    static List<sked__Job_Product__c> getJobProductToDelete(JobProductModel jpModel) {
        List<sked__Job__c> skedJobs = queryRecurringJobWithProduct(jpModel);
        List<sked__Job_Product__c> jobProductToDelete = new List<sked__Job_Product__c>();
        Map<Id, Map<Id, List<sked__Job_Product__c>>> job_Product_JobProduct_Map = buildJobToProductMap(skedJobs);
        for ( sked__Job__c job : skedJobs ) {
            Map<Id, List<sked__Job_Product__c>> product_JobProduct_Map = job_Product_JobProduct_Map.get(job.Id);   
            if ( product_JobProduct_Map.containsKey(jpModel.productId) ) {                
                List<sked__Job_Product__c> jobProducts = product_JobProduct_Map.get(jpModel.productId);
                jobProductToDelete.addAll(jobProducts);
                
            }
        }
        System.debug('jobProductToDelete#' + jobProductToDelete);
        return jobProductToDelete;
    }
    
    static Map<Id, Map<Id, List<sked__Job_Product__c>>> buildJobToProductMap(List<sked__Job__c> skedJobs) {
        Map<Id, Map<Id, List<sked__Job_Product__c>>> job_Product_JobProduct_Map = new Map<Id, Map<Id, List<sked__Job_Product__c>>>();
        for ( sked__Job__c job : skedJobs ) {
            if ( !job_Product_JobProduct_Map.containsKey(job.Id) ) {
                job_Product_JobProduct_Map.put(job.Id, new Map<Id, List<sked__Job_Product__c>>() );
            }
            Map<Id, List<sked__Job_Product__c>> product_JobProduct_Map = job_Product_JobProduct_Map.get(job.Id);
            for ( sked__Job_Product__c jobProduct : job.sked__Job_Products__r ) {
                if ( !product_JobProduct_Map.containsKey(jobProduct.sked__Product__c) ) {
                    product_JobProduct_Map.put(jobProduct.sked__Product__c, new List<sked__Job_Product__c>() ); 
                }
                product_JobProduct_Map.get(jobProduct.sked__Product__c).add(jobProduct);
            }            
        }
        return job_Product_JobProduct_Map;
    }
    
    static List<sked__Job__c> queryRecurringJobWithProduct( JobProductModel jpModel ) {
        sked__Job__c skedJob = [SELECT Id, sked__Start__c, sked__Recurring_Schedule__c FROM sked__Job__c WHERE Id = :jpModel.jobId ];
        List<sked__Job__c> skedJobs = [SELECT Id,
                                       (SELECT Id, sked__Product__c, sked_Description__c, sked__Qty__c, sked_Day_Of_Week__c, sked_Price__c, sked_Unit_Price__c, sked_Package_Name__c, sked_Package__c
                                        FROM sked__Job_Products__r
                                       )
                                       FROM sked__Job__c
                                       WHERE sked__Start__c > :skedJob.sked__Start__c
                                       AND sked__Recurring_Schedule__c = :skedJob.sked__Recurring_Schedule__c
                                       AND Id != :skedJob.Id];
        return skedJobs;
    }
    
    global class skedResponse {
    
        public boolean success      {get;set;}
        public string  message      {get;set{
            message = value;
            if( message.contains('INACTIVE_OWNER_OR_USER,') ) message = message.substringBetween('INACTIVE_OWNER_OR_USER,', ':');           
        }}
        public string  devMessage   {get;set;}
        public string  errorMessage   {get;set;}
        public object  data         {get;set;}
        
        /*
        * contructor
        */
        public skedResponse() {
            this.success = false;
            this.message = '';
            this.errorMessage = '';
            this.data = null;
        }
        
        /*
        * methods go here
        */
        
        public void getErrorMessage(Exception ex){
            this.devMessage = ex.getStackTraceString();
            this.errorMessage    = ex.getMessage();
        }                
    }
    
    public class JobProductModel {
        public Id id;
        public Id jobId;
		public Id productId;
        public String description;
        public Decimal quantity;
        public Decimal price;
        public String weekday;
        public Decimal unitPrice;
        public String packageName;
        public Id packageId; 
    }
    
    global class ServiceRequestModel {
        public Boolean applyForAll;
        public JobProductModel product;
    }
    
}
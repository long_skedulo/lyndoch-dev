// aljsInit
(function(window, $) {
  if (window.ASSET_PATH) {
    var assetsLocation = window.location.origin + window.ASSET_PATH.slice(0, -1)
    console.log('aljsInit: ', assetsLocation)
    $.aljsInit({
      assetsLocation: assetsLocation,
      scoped: true
    })
  }
})(window, $);

(function(angular, jQuery) {
  
  angular.module('ui.autocomplete', [])

    .directive('autocompleteSingle', [function() {
      return {
        restrict: 'A',
        scope: {
          getOptions: '=autocompleteSingle',
          onChange: '=',
          minLength: '@'
        },
        link: function(scope, el) {
          // deps: jquery ui autocomplete
          el.autocomplete({
            delay: 300,
            minLength: parseInt(scope.minLength),

            source: function(request, response) {
              var val = el.val()
              scope.getOptions(val)
                .then(function(results) {
                  console.log('getOptions:', results)
                  response(results)
                })
            },

            select: function(event, ui) {
              // console.log('select', ui.item, event)
              scope.onChange(ui.item, el.data('index'))
            }
          })

          el.prev('img').on('click', function() {
            el.focus()
          })
        }
      }
    }])

})(angular, jQuery);
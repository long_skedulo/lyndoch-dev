public class skedTestDataFactory {
    
    /*
    *
    */
    public static void setupCustomSettings(){
        list<skedRACConfigs__c> configs = new list<skedRACConfigs__c>();
        configs.add(new skedRACConfigs__c(Name='Recurring_Job_Replication_Delay_Time', Value__c='10'));      
		configs.add(new skedRACConfigs__c(Name='Cloning_Job_Related_List ', Value__c='Products '));
        insert configs;

        //insert new sked_Roster_Setting__c (SetupOwnerId=UserInfo.getOrganizationId(), Period__c =14, Start_Date__c =System.now(), No_Of_Days_Before_Next_Roster__c =0, Timesheet_Type__c ='fortnight');

    }
    /*
    * Initializes a list of Account
    */
    public static List<Account> createAccounts(String basename, Integer count){
        List<Account> records = new List<Account>();
        for( Integer i=0; i< count; i++ ){
            Account reccord = new Account(   
                                            Name            = basename + String.valueOf(i),
                                            ShippingStreet  = '79 McLachlan St Fortitude Valley'
                                        ) ;
            
            records.add( reccord);
        }
        return records;
    }

    /*
    * Initializes a list of Job
    */
    public static List<sked__Job__c> createJobs(Id accountId, Id regionId, Id groupEventId, Integer count){
        List<sked__Job__c> records = new List<sked__Job__c>();
        for( Integer i=0; i< count; i++ ){
            sked__Job__c reccord = new sked__Job__c( 
                                            sked__Account__c        = accountId,
                                            sked__Start__c          = System.now().addHours(9+i),
                                            sked__Finish__c         = System.now().addHours(10+i),
                                            sked__Duration__c       = 60,
                                            sked__Region__c         = regionId,
                                            sked__Address__c        = '79 mclachlan'
                                        ) ;
            
            records.add( reccord);
        }
        return records;
    }

    /*
    * Allocate a job to a resource
    */
    public static sked__Job_Allocation__c allocateJob(Id jobId, Id resourceId){
        return new sked__Job_Allocation__c(
                sked__Job__c            = jobId,
                sked__Resource__c       = resourceId,
                sked__Assigned_To__c    = resourceId,
                sked__Time_Start_Travel__c   = System.now().addHours(-2),
                sked__Time_In_Progress__c    = System.now().addHours(-1),
                sked__Time_Completed__c    = System.now()
            );
    }

    /*
    * Initializes a Region
    */
    public static sked__Region__c createRegion(String name, String timezone){
        
        sked__Region__c region = new sked__Region__c(
            Name = name,
            sked__Timezone__c = timezone==null?'Australia/Melbourne':timezone,
            sked__Country_Code__c = 'AU'
        );
            
        return region;
    }

    /*
    * Initializes a Location
    */
    public static sked__Location__c createLocation(String name, Id accountId, Id regionId){
        
        sked__Location__c location = new sked__Location__c(
            Name                = name,
            sked__Account__c    = accountId,
            sked__Region__c     = regionId
        );
            
        return location;
    }

    /*
    * Initializes a Contact
    */
    public static Contact createContact(Id accountId, String name){
        return new Contact(
                FirstName       = name,
                LastName        = name,
                AccountId       = accountId, 
                Birthdate       = System.today().addDays(-10000),
                otherStreet     = 'Test',
                otherCity       = 'Test',
                otherState      = 'Test',
                otherPostalCode = 'Test',
                otherCountry    = 'Test',
                Phone           = '1234567890',
                Email           = 'test@test.com'
            );
    }
    
    /*
    * Initializes a Product
    */
    public static Product2 createProduct(String name, String code ) {
        Product2 product = new Product2(
            Name = name,
            ProductCode = code,
            IsActive = true,
            Rate_Type__c = getPickListValues('Product2', 'Rate_Type__c').get(0)
        );
        return product;
    }       
    
    /*
    * Initializes a Job Product
    */
    public static sked__Job_Product__c createJobProduct(Id jobId, Id prodId, Integer qty) {
        return new sked__Job_Product__c(
                sked__Job__c   = jobId,
                sked__Product__c = prodId,
                sked__Qty__c = qty
            );
    }

    /*
    * Initializes a Resource
    */
    public static sked__Resource__c createResource(String name, Id userId, Id regionId){
        return new sked__Resource__c(
                name                    = name,
                sked__Resource_Type__c  = 'Person',
                sked__Primary_Region__c = regionId,
                sked__Category__c       = 'Customer Service',
                sked__Country_Code__c   = 'AU',
                sked__Home_Address__c   = '24 Tuckett Rd, Salisbury, Queensland, AUS',
                sked__Is_Active__c      = true,
                sked__Weekly_Hours__c   = 40,
                sked__User__c           = userId
            );
    }
    
    public static sked__Recurring_Schedule__c  createRecurringSchedule() {
        return new sked__Recurring_Schedule__c(              
            );
    }
    
    public static List<string> getPickListValues(string objectApiName, string fieldApiName) {
        List<string> results = new List<string>();
        
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectApiName);
        DescribeSObjectResult objDescribe = targetType.getDescribe();
        map<String, SObjectField> mapFields = objDescribe.fields.getmap();
        SObjectField fieldType = mapFields.get(fieldApiName);
        DescribeFieldResult fieldResult = fieldType.getDescribe();
        
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple) {
            results.add(f.getValue());
        }       
        return results;
    }
}
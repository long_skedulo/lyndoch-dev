/**
 * Created by Cain on 28/08/2018.
 */

public with sharing class ExtractExtension
{

    public ExtractExtension(ApexPages.StandardController ctrlr)
    {
        Extract  = (Extract__c)ctrlr.getRecord();
    }

    public Extract__c Extract {get;set;}

    String CSVHEADER_FIN = 'AccountCode,Client Referrence Number,Department,Package,Rate Type,Unit Of Measure,Rate,GST,Client Name, Quantity Delivered, Service, Date\n';
    //String CSVHEADER_PAYROLL = 'EmployeeCode,Date,StartTime,EndTime,,,,,WorkAreaCode,,,,,,,,,,,,,,,,,,\n';
    String CSVHEADER_PAYROLL = 'EmployeeCode,Date,StartTime,EndTime,ReasonCode,BreakAmount,BreaskCode,RequirementCode,WorkAreaCode,LocationCode,DepartmentCode,Dim1Code,Dim2Code,Dim3Code,Dim4Code,PositionCode,TaskCode,ShiftCode,ShiftGroupCode,Type1Code,Type2Code,Type3Code,Type4Code,AwardCode,RateCode,RateAmount,Factor\n';


    Id FinExtractRTId  = Schema.getGlobalDescribe().get('Extract__c').getDescribe().getRecordTypeInfosByName().get('Finance Extract').getRecordTypeId();

    public PageReference saveOverride()
    {
        try
        {
            UPSERT Extract;

            if(Extract.RecordTypeId == FinExtractRTId)
            {
                generateFinExtract();
            }
            else
            {
                generatePayrollExtract();
            }

            return new PageReference('/' + Extract.Id);
        }
        catch(Exception ex)
        {
            ApexPages.addMessages(ex);
            return null;
        }

    }

    private void generateFinExtract()
    {
        List <sked__Job_Product__c> jpList = [SELECT Name,
                                                    sked__Job__r.Name,
                                                    sked__Job__r.sked__Job_Status__c,
                                                    sked__Job__r.sked__Contact__r.Name,
                                                    sked__Job__r.sked__Contact__r.Lyndoch_Account_Code__c,
                                                    sked__Job__r.sked__Contact__r.Client_Reference_Number__c,
                                                    sked__Job__r.sked__Start__c,
                                                    sked__Job__r.sked__Finish__c,
                                                    sked__Product__r.Name,
                                                    sked__Product__r.Department__c,
                                                    sked__Product__r.Rate_Type__c,
                                                    sked__Product__r.QuantityUnitOfMeasure,
                                                    sked_Package__r.Name,
                                                    sked_Unit_Price__c,
                                                    sked__Qty__c,
                                                    GST_Amount__c
                                                FROM sked__Job_Product__c
                                                WHERE sked__Job__r.sked__Job_Status__c = 'Complete'
                                                    AND Extract__c = ''
                                                    AND sked__Job__r.sked__Start__c >= :Extract.From_Date__c
                                                    AND sked__Job__r.sked__Finish__c <= :Extract.To_Date__c];

        //Get map of PriceBook
        List<sked__Job_Product__c> jpToUpdate = new List<sked__Job_Product__c>();
        String csvContent='';
        for(sked__Job_Product__c jp : jpList)
        {
            csvContent += GetLineFin(jp);
            jp.Extract__c = Extract.Id;

            jpToUpdate.add(jp);
        }

        if(SaveFile('Financial Extract',CSVHEADER_FIN, csvContent))
        {
            //Update Job Product records with link to Extract
            if(jpToUpdate.size() > 0)
            {
                UPDATE jpToUpdate;
            }
        }
        else
        {
            Throw new CustomException( 'No records returned');
        }
    }

    private void generatePayrollExtract()
    {
        List<sked__Job_Allocation__c> jaList = [SELECT  Name,
                                                    sked__Time_Checked_In__c,
                                                    sked__Time_Completed__c,
                                                    sked__Resource__r.Work_Area_Code__c,
                                                    sked__Resource__r.Employee_Code__c,
                                                    sked__Resource__r.sked__Working_Hour_Type__c,
                                                    sked__Job__r.sked__Job_Status__c,
                                                    sked__Job__r.sked__Start__c,
                                                    sked__Job__r.sked__Finish__c
                                                FROM sked__Job_Allocation__c
                                                WHERE sked__Job__r.sked__Job_Status__c = 'Complete'
                                                    AND sked__Status__c = 'Complete'
                                                    AND Extract__c = ''
                                                    AND sked__Job__r.sked__Start__c >= :Extract.From_Date__c
                                                    AND sked__Job__r.sked__Finish__c <= :Extract.To_Date__c];

        List<sked__Resource_Shift__c> rsList = [SELECT  Name,
                                                        sked__Shift__r.sked__Start__c,
                                                        sked__Shift__r.sked__End1__c,
                                                        sked__Resource__r.Work_Area_Code__c,
                                                        sked__Resource__r.Employee_Code__c,
                                                        sked__Resource__r.sked__Working_Hour_Type__c,
                                                        sked__Resource__r.sked__Category__c,
                                                        sked__Shift__r.sked__Is_Draft__c
                                                FROM sked__Resource_Shift__c
                                                WHERE sked__Shift__r.sked__Is_Draft__c = false
                                                AND Extract__c = ''
                                                AND sked__Shift__r.sked__Start__c >= :Extract.From_Date__c
                                                AND sked__Shift__r.sked__End1__c <= :Extract.To_Date__c];

        List<sked__Activity__c> activityList = [SELECT  id,
                                                        sked__Start__c,
                                                        sked__End__c,
                                                        sked__Resource__r.Employee_Code__c,
                                                        sked__Resource__r.Work_Area_Code__c,
                                                        sked__Type__c
                                                FROM sked__Activity__c
                                                WHERE sked__Start__c >= :Extract.From_Date__c
                                                AND  sked__End__c <= :Extract.To_Date__c
                                                AND Extract__c = ''];

        List<sked__Job_Allocation__c> jaToUpdate = new List<sked__Job_Allocation__c>();
        List<sked__Resource_Shift__c> rsToUpdate = new List<sked__Resource_Shift__c>();
        List<sked__Activity__c> actToUpdate = new List<sked__Activity__c>();
        String csvContent='';

        //Jobs
        for(sked__Job_Allocation__c ja : jaList)
        {
            if(ja.sked__Resource__r.sked__Working_Hour_Type__c == 'Availability')
            {
                csvContent += GetLinePayRollJobAlloc(ja);
                ja.Extract__c = Extract.Id;

                jaToUpdate.add(ja);
            }
        }

        //Shifts
        for(sked__Resource_Shift__c rs : rsList)
        {
            if(rs.sked__Resource__r.sked__Working_Hour_Type__c == 'Shift')
            {
                csvContent += GetLinePayRollResShift(rs);
                rs.Extract__c = Extract.Id;

                rsToUpdate.add(rs);
            }
        }

        //Activities
        for(sked__Activity__c act: activityList){
            csvContent += GetLinePayRollActivity(act);
            act.Extract__c = Extract.Id;

            actToUpdate.add(act);
        }

        if(SaveFile('Payroll Extract',CSVHEADER_PAYROLL, csvContent))
        {
            System.debug('Inside If loop==> ' + rsToUpdate);
            //Update Job Allocation records with link to Extract
            if(jaToUpdate.size() > 0)
            {
                UPDATE jaToUpdate;
            }

            //Update Resource Shift records with link to Extract
            if(rsToUpdate.size() > 0)
            {
                UPDATE rsToUpdate;
            }

            //Update Activity records with link to Extract
            if(actToUpdate.size() > 0)
            {
                UPDATE actToUpdate;
            }
        }
        else
        {
            Throw new CustomException( 'No records returned');
        }
    }

    private String GetLineFin(sked__Job_Product__c jp)
    {
        String csvContent='',accCode='',crn='',uom='',startDate='';
        System.debug('jp: ' + jp);

        if(!String.isBlank(jp.sked__Job__r.sked__Contact__r.Lyndoch_Account_Code__c))
        {
            accCode = jp.sked__Job__r.sked__Contact__r.Lyndoch_Account_Code__c;
        }

        if(!String.isBlank(jp.sked__Job__r.sked__Contact__r.Client_Reference_Number__c))
        {
            crn = jp.sked__Job__r.sked__Contact__r.Client_Reference_Number__c;
        }

        if(!String.isBlank(jp.sked__Product__r.QuantityUnitOfMeasure))
        {
            uom = jp.sked__Product__r.QuantityUnitOfMeasure;
        }

        if( jp.sked__Job__r.sked__Start__c != NULL)
        {
            startDate = jp.sked__Job__r.sked__Start__c.date().format();
        }
        csvContent = accCode + ',' + crn + ',' +
                jp.sked__Product__r.Department__c + ',' + jp.sked_Package__r.Name + ',' + jp.sked__Product__r.Rate_Type__c + ',' +
                uom + ',' + jp.sked_Unit_Price__c + ','  + jp.GST_Amount__c + ',' +
                jp.sked__Job__r.sked__Contact__r.Name + ',' + jp.sked__Qty__c + ',' + jp.sked__Product__r.Name + ',' + startDate + '\n';

        return csvContent;
    }

    private String GetLinePayRollJobAlloc(sked__Job_Allocation__c ja)
    {
        String csvContent='',startDate='',startTime='',endTime='',workAreaCode='',employeeCode='';
        System.debug('ja: ' + ja);

        if( ja.sked__Time_Checked_In__c != NULL)
        {
            startDate = ja.sked__Time_Checked_In__c.date().format();
            startTime = String.valueOf(MinutesPastMidnight(ja.sked__Job__r.sked__Start__c));

            System.debug('ja.sked__Time_Checked_In__c: ' + ja.sked__Job__r.sked__Start__c);
            System.debug('startDate: ' + startDate);
        }
        if(ja.sked__Time_Completed__c != NULL)
        {
            endTime = String.valueOf(MinutesPastMidnight(ja.sked__Job__r.sked__Finish__c));
        }

        if(!String.isBlank(ja.sked__Resource__r.Work_Area_Code__c))
        {
            workAreaCode = ja.sked__Resource__r.Work_Area_Code__c;
        }

        if(!String.isBlank(ja.sked__Resource__r.Employee_Code__c))
        {
            employeeCode = ja.sked__Resource__r.Employee_Code__c;
        }

        csvContent += employeeCode + ',' + startDate + ',' + startTime + ',' + endTime + ',,,,,' + workAreaCode + ',,,,,,,,,,,,,,,,,,' + '\n';

        return csvContent;
    }

    private String GetLinePayRollResShift(sked__Resource_Shift__c rs)
    {
        String csvContent='',startDate='',startTime='',endTime='',workAreaCode='',employeeCode='';
        System.debug('rs: ' + rs);
        if( rs.sked__Shift__r.sked__Start__c != NULL)
        {
            startDate = rs.sked__Shift__r.sked__Start__c.date().format();
            //startTime = rs.sked__Shift__r.sked__Start__c.format('HH:mm:ss');
            startTime = String.valueOf(MinutesPastMidnight(rs.sked__Shift__r.sked__Start__c));

            System.debug('rs.sked__Shift__r.sked__Start__c: ' + rs.sked__Shift__r.sked__Start__c);
            System.debug('startDate: ' + startDate);
        }
        if(rs.sked__Shift__r.sked__End1__c != NULL)
        {
            Long shiftDuration = ((((rs.sked__Shift__r.sked__End1__c.getTime() - rs.sked__Shift__r.sked__Start__c.getTime())/1000)/60)/60);
            System.debug('rs.sked__Shift__r.sked__End1__c: ' + rs.sked__Shift__r.sked__End1__c);
            if(shiftDuration > 4 && (rs.sked__Resource__r.sked__Category__c == 'Gardener' || rs.sked__Resource__r.sked__Category__c == 'Maintenance')){
                endTime = String.valueOf(MinutesPastMidnight(rs.sked__Shift__r.sked__End1__c.addMinutes(-30)));
            }
            else{
                endTime = String.valueOf(MinutesPastMidnight(rs.sked__Shift__r.sked__End1__c));
            }
            System.debug('endTime: ' + endTime);
        }

        if(!String.isBlank(rs.sked__Resource__r.Work_Area_Code__c))
        {
            workAreaCode = rs.sked__Resource__r.Work_Area_Code__c;
        }

        if(!String.isBlank(rs.sked__Resource__r.Employee_Code__c))
        {
            employeeCode = rs.sked__Resource__r.Employee_Code__c;
        }

        csvContent += employeeCode + ',' + startDate + ',' + startTime + ',' + endTime + ',,,,,' + workAreaCode + ',,,,,,,,,,,,,,,,,,' + '\n';

        return csvContent;
    }

    //creating csv for activities
    private String GetLinePayRollActivity(sked__Activity__c act)
    {
        String csvContent='',startDate='',startTime='',endTime='',workAreaCode='',employeeCode='',shiftcode='';
        Map<String,String> shiftCodesMap = new Map<String,String> {'Annual Leave' => 'L.ANNL', 'ADO Paid' => 'L.ADOP', 'Carers Leave' => 'L.CARE',
        'Carers Leave No Certificate' => 'L.CLNC', 'Department Orientation' => 'L.DEPT', 'Jury Leave' => 'L.JURY', 'Long Service Leave' => 'L.LSLL',
        'Paid Parental Leave' => 'L.PARE', 'Public Holiday Not Worked' => ' L.PHNW', 'Professional Development' => 'L.PROF',
        'Sick Leave with Certificate' => 'L.SLWC', 'Sick Leave No Certificate' => 'L.SLNC' , 'Study Leave' => 'L.STUD', 'Work Cover Leave' => 'L.WORK',
        'Leave Without Pay' => 'U.LWOP', 'Unpaid Parental Leave' => 'U.PARE','Work Cover - Total Incapacity' => '0981',
        'Work Cover - Partial Incapacity' => '0982'};
        System.debug('act: ' + act);
        if( act.sked__Start__c != NULL)
        {
            startDate = act.sked__Start__c.date().format();

            startTime = String.valueOf(MinutesPastMidnight(act.sked__Start__c));
        }
        if(act.sked__End__c != NULL)
        {
            endTime = String.valueOf(MinutesPastMidnight(act.sked__End__c));
        }

        if(!String.isBlank(act.sked__Resource__r.Work_Area_Code__c))
        {
            workAreaCode = act.sked__Resource__r.Work_Area_Code__c;
        }

        if(!String.isBlank(act.sked__Resource__r.Employee_Code__c))
        {
            employeeCode = act.sked__Resource__r.Employee_Code__c;
        }

        if(shiftCodesMap.containsKey(act.sked__Type__c))
        {
            shiftcode = shiftCodesMap.get(act.sked__Type__c);
        }

        csvContent += employeeCode + ',' + startDate + ',' + startTime + ',' + endTime + ',,,,,' + workAreaCode + ',,,,,,,,,"'  + shiftcode + '",,,,,,,,,' + '\n';
        System.debug('shiftcode' + shiftcode);
        System.debug('csvContent===>' + csvContent);

        return csvContent;
    }

    private Boolean SaveFile(String fileType, String Header, String fileContent)
    {
        Boolean savedOk = false;
        String fileName = fileType + '_' + System.now().format();

        if(!String.IsEmpty(fileContent))
        {
            System.debug('Save File getting saved!!');
            ContentVersion cv = new ContentVersion();
            cv.Title = fileName;
            cv.PathOnClient = fileName + '.csv';
            cv.VersionData = Blob.valueOf(Header + fileContent);
            INSERT cv;

            Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id].ContentDocumentId;

            //Create ContentDocumentLink
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.ContentDocumentId = conDoc;
            cdl.LinkedEntityId = Extract.Id;
            cdl.ShareType = 'I';
            INSERT cdl;

            savedOk = true;
        }
        System.debug('Save File : savedOk!' + savedOk);
        return savedOk;
    }

    private Integer MinutesPastMidnight(Datetime dt)
    {
        Integer minutesPastMidnight = 0,intHour, intMinute;
        System.debug('dt ==' + dt);
        intHour = dt.hour();
        intMinute = dt.minute();
        System.debug('Hour ==' + intHour);
        System.debug('intMinute ==' + intMinute);
        minutesPastMidnight = intHour * 60 + intMinute;

        if(dt.second() > 0)
        {
            minutesPastMidnight += 1;
        }
        return minutesPastMidnight;
    }
}
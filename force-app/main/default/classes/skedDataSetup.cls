@istest
public class skedDataSetup {
    // set up custom setting
    public static void setupCustomSettings(){
        insert getCustomSettingsData();
    }
    public static List<sObject> getCustomSettingsData() {
        List<sObject> customSettings = new List<sObject>();

        skedConfigs__c skedConfigSettings = new skedConfigs__c(
            sked_Service_Path__c = 'https://test.skedulo.com/job-products/'
        );
        customSettings.add(skedConfigSettings);

        return customSettings;
    }

    // set up common Data test
    public static Map<string, sObject> setupCommonTestData() {
        setupCustomSettings();

        Map<string, sObject> mapTestData = new Map<string, sObject>();

        /*********************************************************1st Level**************************************************/
        List<sObject> firstList = new List<sObject>();
        /*********************************************************Region**************************************************/
        sked__Region__c regionQld = new sked__Region__c(
            Name = 'QLD',
            sked__Timezone__c = 'Australia/Queensland'
        );
        firstList.add(regionQld);
        mapTestData.put('regionQld', regionQld);

        insert firstList;
        /*********************************************************End of 1st level**************************************************/

        /*********************************************************2nd Level**************************************************/
        List<sObject> secondList = new List<sObject>();
        /*********************************************************Job********************************************************/
        DateTime dt = system.now();
        sked__Job__c job1 = new sked__Job__c(
            sked__Region__c = regionQld.Id,
            sked__Start__c = dt,
            sked__Finish__c = dt.addHours(1),
            sked__Duration__c = 60,
            sked__Address__c = '79 mclachlan Fortitude valley',
            sked__Job_Status__c = 'Pending Dispatch'
        );

        secondList.add(job1);
        mapTestData.put('job1', job1);

        insert secondList;

        return mapTestData;
    }
}
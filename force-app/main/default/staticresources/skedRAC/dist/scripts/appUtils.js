(function(window, angular) {

  var getTemplateUrl = function(name) {
      return window.DIST_PATH + 'dist/templates/' + name
  }

  var getIconUrl = function(name, group) {
      if (!group) group = 'utility'
      return window.ASSET_PATH + 'assets/icons/' + group + '-sprite/svg/' + name
  }

  var getIconSrc = function(name, group) {
      if (!group) group = 'utility'
      return window.ASSET_PATH + 'assets/icons/' + group + '/' + name
  }
  
  angular.module('app.utils', [])
    .factory('getTemplateUrl', [function(){
      return getTemplateUrl
    }])
    .factory('getIconUrl', [function(){
      return getIconUrl
    }])
    .factory('getIconSrc', [function(){
      return getIconSrc
    }])

})(window, angular);
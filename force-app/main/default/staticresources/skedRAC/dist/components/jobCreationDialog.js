(function(window, angular, toastr) {

  function AddressModel (isPrimary) {
    this.isPrimary = isPrimary || false
    this.location = null
    this.address = ''
    this.activity = ''
  }

  function JobModel () {
    this.id = null
    this.description = ''
    this.startDate = new Date()
    this.duration = 15
    this.numOfResources = { id: 1, label: 1 }
    this.comment = ''
    this.addresses = [new AddressModel(true)]
  }

  angular.module('app.jobCreation', ['app.utils' ,'ui.sldsModal', 'ui.date', 'app.api', 'svc.geoCode', 'ui.autocomplete'])
    .component('jobCreationDialog', {
      templateUrl: window.DIST_PATH + 'dist/components/jobCreationDialog.html',
      bindings: {
        onSaveSuccess: '&',
        openState: '<',
        country: '@',
        onHide: '&'
      },
      controller: ['$timeout', '$scope', 'getIconUrl', 'getIconSrc', 'getTemplateUrl', 'geoCodeSvc', 'api',
        function ($timeout, $scope, getIconUrl, getIconSrc, getTemplateUrl, geoCodeSvc, api) {
          var $ctrl = this

          $ctrl.$onInit = function () {
            console.log('jobCreationDialog', this)

            $ctrl.state = {
              form: null,
              job: new JobModel()
            }
          }

          $scope.$watch('$ctrl.openState', function (newVal) {
            if (newVal === true) {
              $ctrl.state.form.$setPristine()
              $ctrl.state.job = new JobModel()
            }
          })

          $ctrl.getIconUrl = getIconUrl
          $ctrl.getIconSrc = getIconSrc
          $ctrl.getTemplateUrl = getTemplateUrl

          $ctrl.dateOptions = {
            constrainInput: true,
            dateFormat: "D, d M yy",
            firstDay: 1,
            onSelect: function (value, picker, $el) {
              $timeout(function () {
                $el.blur()
              }, 100)
            }
          }

          $ctrl.getResources = function (max) {
            var output = []
            for (var i=1; i<=max; i++) {
              output.push({ id: i, label: i })
            }
            return output
          }

          $ctrl.handleOnHide = function () {
            $timeout($ctrl.onHide)
          }

          $ctrl.getOptions = function(query) {
            return geoCodeSvc.get(query, $ctrl.country)
          }

          $ctrl.changeOption = function(val, index) {
            $timeout(function() {
              var location = angular.copy(val)
              $ctrl.state.job.addresses[index].location = location
              $ctrl.state.job.addresses[index].address = location.value
              console.log(index, 'address', location)
            }, 100)
          }

          $ctrl.addAddress = function (index) {
            console.log('addAddress', index)
            $ctrl.state.job.addresses.push(new AddressModel())
          }

          $ctrl.removeAddress = function (index) {
            console.log('removeAddress', index)
            $ctrl.state.job.addresses.splice(index,1)
          }

          $ctrl.saveJob = function (state) {
            console.log('saveJob', state)
            state.form.$setSubmitted(true)
            if (state.form.$valid) {
              api.saveJob(angular.toJson(state.job))
                .then(
                  function done(response) {
                    console.log('done', response)
                    $ctrl.onSaveSuccess({ result: response.result })
                    toastr.success('Job saved successfully.')
                    $ctrl.handleOnHide()
                },
                  function fail(response) {
                    toastr.error(response.message)
                })
            }
          }
        }
      ]
    })

})(window, angular, toastr);
global virtual class skedUtils {
    global static string TIMESLOT_KEY_FORMAT = 'dd/MM/yyyy hh:mm a';
    global static string SF_CONTENTVERSION_URL = '';
    global static string DATE_FORMAT = 'yyyy-MM-dd';
    global static string DATE_ISO_FORMAT = '"yyyy-MM-dd"';

    global static Map<string, Set<Date>> getHolidays(Date currentDate) {
        Map<string, Set<Date>> mapHolidays = new Map<string, Set<Date>>();
        List<sked__Holiday__c> skedGlobalHolidays = [SELECT Id, sked__Start_Date__c, sked__End_Date__c
                                                     FROM sked__Holiday__c
                                                     WHERE sked__Global__c = TRUE
                                                     AND sked__End_Date__c >= :currentDate];
        List<sked__Holiday_Region__c> skedRegionHolidays = [SELECT Id, sked__Holiday__r.sked__Start_Date__c, sked__Holiday__r.sked__End_Date__c,
                                                            sked__Region__r.Name
                                                            FROM sked__Holiday_Region__c
                                                            WHERE sked__Holiday__r.sked__End_Date__c >= :currentDate];

        Set<Date> globalHolidays = new Set<Date>();
        for (sked__Holiday__c globalHoliday : skedGlobalHolidays) {
            Date tempDate = globalHoliday.sked__Start_Date__c;
            while (tempDate <= globalHoliday.sked__End_Date__c) {
                globalHolidays.add(tempDate);
                tempDate = tempDate.addDays(1);
            }
        }
        mapHolidays.put(skedConstants.HOLIDAY_GLOBAL, globalHolidays);

        for (sked__Holiday_Region__c regionHoliday : skedRegionHolidays) {
            Set<Date> regionHolidays;
            if (mapHolidays.containsKey(regionHoliday.sked__Region__r.Name)) {
                regionHolidays = mapHolidays.get(regionHoliday.sked__Region__r.Name);
            } else {
                regionHolidays = new Set<Date>();
            }

            Date tempDate = regionHoliday.sked__Holiday__r.sked__Start_Date__c;
            while (tempDate <= regionHoliday.sked__Holiday__r.sked__End_Date__c) {
                regionHolidays.add(tempDate);
                tempDate = tempDate.addDays(1);
            }

            if (!mapHolidays.containsKey(regionHoliday.sked__Region__r.Name)) {
                mapHolidays.put(regionHoliday.sked__Region__r.Name, regionHolidays);
            }
        }
        return mapHolidays;
    }

    public static String buildLikeSearchString(String term) {
        if (String.isBlank(term)) {
            return '%%';
        }

        return '%' + String.escapeSingleQuotes(term).trim() + '%';
    }

}
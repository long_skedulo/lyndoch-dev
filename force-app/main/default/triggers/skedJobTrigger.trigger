trigger skedJobTrigger on sked__Job__c (after insert) {
    skedJobTriggerHandler jobHandler = new skedJobTriggerHandler();

    if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            jobHandler.handleAfterInsert(Trigger.new);
        }
    }
}
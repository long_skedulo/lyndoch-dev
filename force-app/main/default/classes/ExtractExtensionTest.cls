/**
 * Created by Cain on 31/08/2018.
 */
@IsTest
public with sharing class ExtractExtensionTest
{
    @IsTest
    public static void test1()
    {
        Product2 p = new Product2();
        p.Name = 'test';
        p.Department__c = 'HIP';
        p.Rate_Type__c = 'Weekday';
        p.QuantityUnitOfMeasure = 'Hour';
        INSERT P;

        sked__Region__c regionQld = new sked__Region__c();
        regionQld.Name = 'QLD';
        regionQld.sked__Timezone__c = 'Australia/Queensland';
        INSERT regionQld;

        DateTime dt = system.now();

        sked__Job__c jb = new sked__Job__c();
        jb.sked__Region__c = regionQld.Id;
        jb.sked__Start__c = dt;
        jb.sked__Finish__c = dt.addHours(1);
        jb.sked__Duration__c = 60;
        jb.sked__Address__c = '79 mclachlan Fortitude valley';
        jb.sked__Job_Status__c = 'Pending Dispatch';
        INSERT jb;

        //PriceBook
        Pricebook2 pb = new Pricebook2();
        pb.Name = 'Standard Price Book';
        pb.IsActive = true;
        pb.Start_Date__c = Date.newInstance(2018,1,1);
        //pb.IsStandard = true;
        INSERT pb;

        sked__Job_Product__c jp = new sked__Job_Product__c();
        jp.sked__Job__c = jb.Id;
        jp.sked__Product__c = p.Id;
        jp.sked__Qty__c = 3;
        jp.sked_Price__c = 15.00;
        jp.sked_Unit_Price__c = 5.00;
        jp.sked_Package__c = pb.Id;
        INSERT jp;

        Contact c = new Contact();
        c.LastName = 'Name';
        INSERT c;

        Profile pf = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                LocaleSidKey='en_US', ProfileId = pf.Id,
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');

        sked__Resource__c res = new sked__Resource__c();
        res.Name = 'Test Res';
        res.sked__Primary_Region__c = regionQld.Id;
        res.Employee_Code__c = '1006';
        res.Work_Area_Code__c = '34';
        res.sked__User__c = u.Id;
        res.sked__Working_Hour_Type__c = 'Availability';
        INSERT res;

        sked__Job_Allocation__c ja = new sked__Job_Allocation__c();
        ja.sked__Resource__c = res.Id;
        ja.sked__Job__c = jb.Id;
        ja.sked__Time_Checked_In__c = dt;
        ja.sked__Time_Completed__c = dt;
        ja.sked__Status__c = 'Complete';
        INSERT ja;

        jb.sked__Job_Status__c = 'Complete';
        UPDATE jb;

        sked__Shift__c s = new sked__Shift__c();
        s.sked__Region__c = regionQld.Id;
        s.sked__Start__c = dt;
        s.sked__End1__c = dt.addHours(10);
        s.sked__Is_Draft__c = false;
        s.sked__Duration__c = 600;
        INSERT s;

        sked__Resource__c res1 = new sked__Resource__c();
        res1.Name = 'Test Res';
        res1.sked__Primary_Region__c = regionQld.Id;
        res1.Employee_Code__c = '1006';
        res1.Work_Area_Code__c = '34';
        res1.sked__User__c = u.Id;
        res1.sked__Working_Hour_Type__c = 'Shift';
        INSERT res1;

        sked__Resource_Shift__c rs = new sked__Resource_Shift__c();
        rs.sked__Resource__c = res1.Id;
        rs.sked__Shift__c = s.Id;
        INSERT rs;

        sked__Activity__c act = new sked__Activity__c();
        act.sked__Address__c = 'Test Location';
        act.sked__Type__c = 'Annual Leave';
        act.sked__Start__c = dt;
        act.sked__End__c = dt.addHours(2);
        act.sked__Resource__c = res1.Id;
        INSERT act;


        Extract__c ex = new Extract__c();
        ex.RecordTypeId =  Schema.getGlobalDescribe().get('Extract__c').getDescribe().getRecordTypeInfosByName().get('Finance Extract').getRecordTypeId();
        ex.From_Date__c = System.Today();
        ex.To_Date__c =System.Today().addDays(2);
        ApexPages.StandardController stdController = new ApexPages.StandardController(ex);

        ExtractExtension ee = new ExtractExtension(stdController);
        ee.saveOverride();

        ex = new Extract__c();
        ex.RecordTypeId =  Schema.getGlobalDescribe().get('Extract__c').getDescribe().getRecordTypeInfosByName().get('Payroll Extract').getRecordTypeId();
        ex.From_Date__c = System.Today().addDays(-1);
        ex.To_Date__c =System.Today().addDays(2);
        stdController = new ApexPages.StandardController(ex);

        ee = new ExtractExtension(stdController);
        ee.saveOverride();

        Test.startTest();
        CustomException e = new CustomException('blah');
        Test.stopTest();
    }
}
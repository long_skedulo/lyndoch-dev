/**
 * Created by Cain on 28/08/2018.
 */

public with sharing class GenerateExtract
{
    public List<Contact> contList{get;set;}

    public GenerateExtract()
    {
        contList = [SELECT Id, NAME FROM Contact];
    }
}
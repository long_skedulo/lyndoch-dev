global class skedAsyncJob implements Database.Batchable<sObject>, Database.AllowsCallouts, Schedulable, Database.Stateful{

    public String action;
    public Object param;
    public Object data;
    public List<Exception> exceptions;
    public skedIAsyncJobManager jobManager;
    //Batchable job variable
    public List<sObject> objects;
    //Schedulable job variable
    public Object context;
    public boolean isBatchable;
    //Serial Job variables
    public boolean isSerialJob;
    public String token;
    public boolean isCompleted;
    public String jobId;
    
    public skedAsyncJob(){
        init();
    }

    public skedAsyncJob(String action, Object param){
        init();
        this.action     = action;
        this.param      = param;
    }

    public void init(){
        this.objects        = new List<SObject>();
        this.isSerialJob    = false;
        exceptions          = new List<Exception>();
        isBatchable         = false;
        isCompleted         = false;
    }

    //Execute job
    public virtual void execute(){
        jobManager.preExec(this);
        try{
            if(this.isBatchable){
                jobManager.executeBatch(this);
            }else{
                jobManager.executeJob(this);
            }
        }catch(Exception ex){
            this.exceptions.add(ex);
            System.debug( LoggingLevel.ERROR, 'Async Job Exception: ' + this.action + ' '  + this.param!=null? JSON.serialize(this.param):'' );
        }
        if(!this.isBatchable) isCompleted = true;
        jobManager.postExec(this);
    }

    //======== OVERRIDES INTERFACE METHODS ============

    // Override Schedulable.execute()
    public void execute(SchedulableContext context) {
        this.context = context;
        this.execute();
        System.abortJob(context.getTriggerId());
    }

    // Override Batchable.start() method
    global List<sObject> start(Database.BatchableContext context) {
        this.context = context;
        return jobManager.startBatch(this);
    }
    // Override Batchable.execute() method
    global void execute(Database.BatchableContext context, List<sObject> objects) {
        this.isBatchable = true;
        this.objects = objects;
        this.context = context;
        this.execute();
    }
    // Override Batchable.finish() method
    global void finish(Database.BatchableContext context) {
        this.context = context;
        isCompleted = true;
        jobManager.finishBatch(this);
    }
}
global virtual class skedModels {

    global virtual class resource {
        global string id {get;set;}
        global string name {get;set;}
        global string category {get;set;}
        global string resourceType {get;set;}
        global string photoUrl {get;set;}
        global string regionId {get;set;}
        global Integer certificationMatch {get;set;}
        global Boolean hasRequiredRole {get;set;}
        global string employmentType {get;set;}
        global string timezoneSidId {get;set;}
        global string regionName {get;set;}
        global string userId {get;set;}
        global string subCategory {get;set;}

        global transient Location geoLocation {get;set;}
        global transient Map<Id, resourceTag> mpTags {get;set;}
        global transient Boolean isFullTime;
        global transient Boolean isAsset;

        global resource() {

        }

        global resource(sked__Resource__c skedResource) {
            this(skedResource, '');
        }

        global resource(sked__Resource__c skedResource, String requiredRole) {
            this.id = skedResource.Id;
            this.name = skedResource.Name;
            this.regionId = skedResource.sked__Primary_Region__c;
            this.photoUrl = skedResource.sked__User__c  != null ? skedResource.sked__User__r.SmallPhotoUrl : '';
            this.resourceType = skedResource.sked__Resource_Type__c;
            this.category = skedResource.sked__Category__c;
            this.subCategory = 'Sub category';
            this.geoLocation = skedResource.sked__GeoLocation__c;
            this.mpTags = new Map<Id, resourceTag>();
            this.hasRequiredRole = false;
            this.isAsset = skedConstants.RESOURCE_TYPE_ASSET.equalsIgnoreCase(this.resourceType);

            this.timezoneSidId = skedResource.sked__Primary_Region__c != null ? skedResource.sked__Primary_Region__r.sked__Timezone__c : '';

            if (skedResource.sked__ResourceTags__r != NULL && !skedResource.sked__ResourceTags__r.isEmpty()) {
                for (sked__Resource_Tag__c skedResourceTag : skedResource.sked__ResourceTags__r) {
                    resourceTag tag = new resourceTag();
                    tag.resourceId = skedResourceTag.sked__Resource__c;
                    tag.tagId = skedResourceTag.sked__Tag__c;
                    tag.expiryDate = skedResourceTag.sked__Expiry_Date__c;
                    mpTags.put(tag.tagId, tag);
                }
            }
        }
    }



    global class resourceTag {
        global string resourceId {get;set;}
        global string tagId {get;set;}
        global transient DateTime expiryDate {get;set;}
    }

    global virtual class OptionModel {
        global string id;
        global string name;
        global string label;

        global OptionModel() {

        }

        global OptionModel(String id, String name) {
            this.id = id;
            this.name = name;
            this.label = name;
        }
    }
}
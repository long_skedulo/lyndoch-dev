public class skedJobTriggerHandler {
	public void handleAfterInsert(List<sked__Job__c> jobs) {
        updateAddServiceJob(jobs);
        cloneJobRealtedListForRecurringJobs(jobs);
    }
    
    void cloneJobRealtedListForRecurringJobs(List<sked__Job__c> newJobs) {
        Set<Id> jobIdsWithRecurring = new Set<Id>();

        for(sked__Job__c job : newJobs) {
            if (job.sked__Recurring_Schedule__c != null) {
                jobIdsWithRecurring.add(job.Id);
            }
        }

        if (!jobIdsWithRecurring.isEmpty()) {
            skedLyndochJobManager.newInstance().register(skedConstants.CLONE_JOB_RELATED_LIST, jobIdsWithRecurring);
        }
    }
    
    void updateAddServiceJob(List<sked__Job__c> jobs) {
        skedConfigs__c config = skedConfigs__c.getOrgDefaults();
        String servicePath = '';
        if (config != null && String.isNotBlank(config.sked_Service_Path__c)) {
            servicePath = config.sked_Service_Path__c;
        }
        List<sked__Job__c> updateAddServiceJob = new List<sked__Job__c>();
        for (sked__Job__c job : jobs) {
            if (String.isNotBlank(servicePath)) {
                sked__Job__c updateJob = new sked__Job__c();
                updateJob.Id = job.Id;
                updateJob.sked_Add_Service__c = servicePath + job.Id;
                updateAddServiceJob.add(updateJob);
            }
        }
        if (!updateAddServiceJob.isEmpty()){
            update updateAddServiceJob;
        }
    }
}
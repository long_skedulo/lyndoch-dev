(function(window, angular, moment, $) {

  if (window.ASSET_PATH) {
    var assetsLocation = window.location.origin + window.ASSET_PATH.slice(0, -1)
    console.log('aljsInit: ', assetsLocation)
    $.aljsInit({
      assetsLocation: assetsLocation,
      scoped: true
    })
  }

  var mod = angular.module('uiDirs', [])

  mod.directive('autocompleteSingle', ['$log', function($log) {
    return {
      restrict: 'A',
      scope: {
        getOptions: '=autocompleteSingle',
        onChange: '=',
        minLength: '@'
      },
      link: function(scope, el) {
        // deps: jquery ui autocomplete
        el.autocomplete({
          delay: 300,
          minLength: parseInt(scope.minLength),

          source: function(request, response) {
            var val = el.val()
            scope.getOptions(val)
              .then(function(results) {
                $log.debug('getOptions:', results)
                response(results)
              })
          },

          select: function(event, ui) {
            scope.onChange(ui.item)
          }
        })

        el.prev('img').on('click', function() {
          el.focus()
        })
      }
    }
  }])

  mod.directive('sldsModal', function() {
    return {
      restrict: 'A',
      scope: {
        'sldsModal': '=',
        'onHide': '='
      },
      link: function(scope, el, attrs) {

        var $firstInput = $(el).find('[data-focus-first]').first()
        var $lastInput = $(el).find('[data-focus-last]').last()

        $(el).on('dismissed.aljs.modal', function() {
          if (scope.onHide) scope.onHide()
        })

        $(el).on('shown.aljs.modal', function() {
          /*set focus on first input*/
          $firstInput.focus()
        })

        /*redirect last tab to first input*/
        $lastInput.on('keydown', function(e) {
          if ((e.which === 9 && !e.shiftKey)) {
            e.preventDefault()
            $firstInput.focus()
          }
        })

        /*redirect first shift+tab to last input*/
        $firstInput.on('keydown', function(e) {
          if ((e.which === 9 && e.shiftKey)) {
            e.preventDefault()
            $lastInput.focus()
          }
        })

        scope.$watch('sldsModal', function(is_show) {
          if (!!is_show) {
            $(el).modal('show')
          } else {
            $(el).modal('dismiss')
          }
        })

      }
    }
  })
})(window, angular, moment, $);

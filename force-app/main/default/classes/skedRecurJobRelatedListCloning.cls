public virtual class skedRecurJobRelatedListCloning extends skedJobRelatedListCloning {
    public skedRecurJobRelatedListCloning() {
        super();
        this.parameter = new RelatedListParameter(skedConfigs.CLONING_JOB_RELATED_LIST);
        System.debug('parameter##' + parameter);
    }

    protected virtual override Id getReferenceKey(sked__Job__c job) {
        return job.sked__Recurring_Schedule__c;
    }

    private Set<Id> getRecurringScheduleIds (List<sked__Job__c> jobs) {
        Set<Id> recurringIds = new Set<Id>();
        for (sked__Job__c job : jobs) {
            if (job.sked__Recurring_Schedule__c != null) {
                recurringIds.add(job.sked__Recurring_Schedule__c);
            }
        }
        return recurringIds;
    }

    public virtual void cloneJobRealtedListForRecurringJobs(Set<Id> jobIdsWithRecurring) {
        List<sked__Job__c> newJobs = [
            SELECT Id, sked__Recurring_Schedule__c
            FROM sked__Job__c
            WHERE Id IN :jobIdsWithRecurring
            FOR UPDATE
        ];

        Set<Id> recurringIds = getRecurringScheduleIds(newJobs);

        if (!recurringIds.isEmpty()) {

            SavePoint sp = Database.setSavepoint();
            try {
                Map<Id,sked__Job__c> mapRSIDToJob = new Map<Id,sked__Job__c>();
                skedObjectQueryBuilder jobQueryBuilder = getJobQueryBuilder();
                jobQueryBuilder.whereClause = ' WHERE sked__Recurring_Schedule__c IN :recurringIds ';
                jobQueryBuilder.extraClause = ' ORDER BY CreatedDate ASC ';

                for(sked__Job__c job : Database.query(jobQueryBuilder.getQuery())) {

                    if(!mapRSIDToJob.containsKey(job.sked__Recurring_Schedule__c)) {
                        mapRSIDToJob.put(job.sked__Recurring_Schedule__c, job);
                    }
                }

                List<sked__Job__c> qualifiedJobs = new List<sked__Job__c>();

                for (sked__Job__c job : newJobs) {
                    if (job.sked__Recurring_Schedule__c != null && mapRSIDToJob.containsKey(job.sked__Recurring_Schedule__c)) {
                        qualifiedJobs.add(job);
                    }
                }

                if (!qualifiedJobs.isEmpty()) {
                    cloneRelatedList(qualifiedJobs, mapRSIDToJob);
                }
            }  catch ( Exception ex ) {
                Database.rollback(sp);
                throw ex;
            }
        }
    }
}